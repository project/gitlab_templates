# GitLab Templates Changelog

## 1.7.3 - 2025-02-14

#3502507 - Tags contained in issue fork triggers pipelines (and should not).\
#3505585 - Create environment variable for the project's own folder.

## 1.7.2 - 2025-02-12

#3504083 - Change `workflow:` into a re-usable reference that can be extended.\
#3505084 - Bump core versions to 11.1.2/10.4.2.\
#3503966 - Make sure each `script:` starts in the intended directory.\
#3503426 - Add Gitlab Templates Downstream testing project.

## 1.7.1 - 2025-01-29

#3502879 - Build task fails on project without `composer.json` file.\
#3473000 - Documentation updates.\
#3502529 - Set `SE_START_XVFB: 'true'` for selenium w3c testing.

## 1.7.0 - 2025-01-17

#3497525 - Only symlink to the project's own files. [Change record](https://www.drupal.org/node/3500320).\
#3425446 - Add variables to configure `allow_failure` in all linting jobs.

## 1.6.14 - 2025-01-13

#3494834 - Add common words not in core dictionaries.\
#3498323 - CSpell: `*.install,*.module` etc should be treated as PHP.\
#3499267 - Remote variables not populated `gitlab-ci-local` usage.\
#3498244 - Coder 8.3.27 released - fix new phpcs faults.\
#3496181 - Add `allow_failure: true` to max PHP variants.\
#3497842 - Bump core versions to 11.1.1/10.4.1.

## 1.6.13 - 2024-12-27

#3496181 - Update PHP_MAX variables to 8.4.\
#3439240 - Cspell: sanitize suggested words for dictionary.

## 1.6.12 - 2024-12-19

#3494834 - Add common words not in core dictionaries.\
#3492697 - Update Stylelint formatter for Stylelint 16.

## 1.6.11 - 2024-12-18

#3397162 - Tweak PHPStan config so paths are always correct and baseline is more usable.\
#3493016 - Update core branches ready for Drupal 11.1.0/10.4.0.\
#3492411 - Show pipeline info and gitlab_templates version in composer log.\
#3491556 - UI variables take precedence over `build.env`.\
#3492927 - Fix regex when parsing `info.yml`.\
#3491871 - GitLab pages job broken.

## 1.6.10 - 2024-12-05

#3463740 - Improve derivation of project name and type.\
#3491863 - Bump D7 version to 7.103.\
#3490191 - Nightwatch pipeline started to fail if the module contains Nightwatch commands.\
#3475974 - Preserve environment variables when running testing jobs.\
#3489372 - Update core stable versions to 10.3.10 and 11.0.9.

## 1.6.9 - 2024-11-21

#3488919 - Bump (security) versions for D7, D10 and D11.\
#3487169 - Set `IMAGE` and `CURL` variables for all subsequent jobs.\
#3488104 - Test-only job should ignore changes to Nightwatch files.\
#3487525 - Customized services not known in 'next minor' jobs.

## 1.6.8 - 2024-11-14

#3486466 - Fixed undefined `_CURL` variables.

## 1.6.7 - 2024-11-13

#3486943 - Repo top-level logo does not show in MkDocs pages.\
#3486466 - Adjust Core phpunit.xml to make it work for Contrib.\
#3439021 - Improve internal eslint and prettier checks.\
#3486983 - Update core stable versions to 10.3.8 and 11.0.7.

## 1.6.6 - 2024-11-11

#3486021 - Unable to run "composer (previous major)" from pipeline UI; no image.\
#3484713 - Use `retry:2` on script failure for composer jobs.\
#3485285 - Give better log information in Pages job.\
#3486124 - Bump core stable versions to 10.3.7 and 11.0.6.

## 1.6.5 - 2024-11-06

#3485054 - Core 11.1.x is released, update next_minor_dev to 11.2.x.\
#3483075 - Update `CORE_PHP_NEXT` to 8.4.\
#3484055 - Do not run the Max PHP jobs when the PHP version is the same as 'current'.\
#3473000 - Documentation pages, consistent language.\
#3482069 - Update default PHP image to `ubuntu-apache` for a higher SQLite version. [Change record](https://www.drupal.org/node/3488684).\
#3481194 - Restore `composer.json` after failed drush installation.

## 1.6.4 - 2024-10-18

#3480767 - `browser_output` is empty when testing with Drupal 11 and `_PHPUNIT_CONCURRENT=0`.\
#3481083 - Test-only changes should not revert changes in key files.\
#3480296 - Only use Composer 2 in Drupal 7 pipelines.

## 1.6.3 - 2024-10-14

#3479986 - `DRUPAL_CORE` not taken into account.\
#3478155 - `cspell` job is not ignoring words with apostrophe.

## 1.6.2 - 2024-10-14

#3480533 - Add Drupal 7 variables for `CORE_LEG_PHP`, `_TARGET_D7_PHP` and `_TARGET_D7_DB_VERSION`.

## 1.6.1 - 2024-10-10

#3479303 - Use stable core versions.\
#3479812 - `upgrade status` job fails due to unmet database requirements.\
#3479581 - Better failure if there are no variables.

## 1.6.0 - 2024-10-07

#3463894 - Update templates so Drupal 11.0 is the default/current branch.

## 1.5.10 - 2024-10-04

#3478044 - Find a better consistent way to override the core version.\
#3477381 - Ignore all vendor git files in artifacts.

## 1.5.9 - 2024-09-24

#3476128 - Multiple improvements to check-versions script.\
#3471235 - Inform jobs about the "composer" job exit status and bail out early.\
#3473051 - CSpell should never be run against patch files.

## 1.5.8 - 2024-09-17

#3462681 - Add W3C compliant JS testing.\
#3458238 - CI installs wrong Drupal version.

## 1.5.7 - 2024-09-02

#3470918 - Core requirement rewriter can result in a bad constraint.\
#3469949 - Internal code check to find non-active documentation links.

## 1.5.6 - 2024-08-26

#3463894 - Temporarily turn `OPT_IN_TEST_NEXT_MAJOR` on for all contrib.\
#3426289 - Cater for empty next/previous minor/major variables.\
#3463894 - Document variants page.\
#3469616 - Nightwatch tests in the test modules are not detected.\
#3447792 - Add `_TARGET_PHP_IMAGE_VARIANT` to cater for newer sqlite versions.\
#3466101 - 'parallel' property type must be integer at test-only `changes.parallel`.\
#3414505 - Allow all sub-modules to be compatible with next_major.\
#3459888 - Upgrade Status: `MODULE_NAME` calculation and theme support.

## 1.5.5 - 2024-07-18

#3461974 - Spellcheck: ignore svg files by default.\
#3439644 - Documentation updates.\
#3458240 - `expand_composer_json.php` should not encode tags.\
#3458411 - CSpell fails when `drupal-dictionary.txt` is missing.\
#3459196 - The upgrade status job should allow adding options to the composer command.

## 1.5.4 - 2024-07-05

#3459198 - Bump CORE_STABLE to 10.3.1.

## 1.5.3 - 2024-07-01

#3457955 - Improve phpunit-tests-exist-rule.\
#3405382 - Override "minimum-stability" for future runs as it needs to be "dev".\
#3456092 - Improve logic for `core_version_requirement` for `NEXT_MAJOR` run.\
#3456414 - Fix incorrect references to `include.drupalci.variables.yml`.\
#3456096 - Additional feedback in `check_versions.php` script.

## 1.5.2 - 2024-06-23

#3456096 - Bump CORE_* versions after 10.3.0 was released.

## 1.5.1 - 2024-06-20

#3455923 - Allow scheduled pipelines from non 'project' namespace.\
#3454363 - CSpell ignores files in hidden folders.\
#3453105 - Include a PHPStan baseline without a custom `phpstan.neon` file.

## 1.5.0 - 2024-06-13

#3449098 - Linting skips files ending in `.theme`, `.profile` and `.engine`.\
#3450701 - Add a variable for `run-tests.sh` concurrency.\
#3453102 - Add a variable to specify PHPStan level.\
#3453001 - Check if composer.lock file exist in composer-lint job.\
#3452898 - Bump core stable to 10.2.7.\
#3450355 - Composer Lint needs to check original `composer.json`.

## 1.4.1 - 2024-05-28

#3449621 - Add rule for project-update-bot to test against next major.

## 1.4.0 - 2024-05-22

#3444789 - Include upgrade status job for all contrib.\
#3447810 - Include for hidden variables cannot be found in remote GitLab instances.\
#3400979 - Change default value for `SYMFONY_DEPRECATIONS_HELPER`.

## 1.3.13 - 2024-05-16

#3444792 - Full path project folder in phpunit.

## 1.3.12 - 2024-05-16

#3444792 - Prepare for PHPUnit 10.\
#3447105 - DB requirements for next major were raised.\
#3343522 - Hide variables that should not be overridden.\
#3444601 - Make `DRUPAL_CORE` and `PHP_VERSION` available in all jobs.\
#3444921 - Run all jobs in 'Check Code', do not halt at first failure.\
#3439644 - Review all documentation pages, part 2.\
#3444778 - Improve the check-versions job output.

## 1.3.11 - 2024-04-23

#3444091 - Offer handy service to extend from with services connected.

## 1.3.10 - 2024-04-23

#3441816 - All variants run the same Drupal version when the pipeline is triggered via web.\
#3442293 - Specify additional CSpell flag words.\
#3442509 - D7 drush failing drupal install.\
#3442120 - D7 pipeline fails with 413 Request Entity Too Large.\
#3440136 - Remove case that was not needed.\
#3441307 - Update logo with GitLab's new version.

## 1.3.9 - 2024-04-16

#3440136 - Improve deprecation process and provide backwards compatibility warnings.\
#3439644 - Review all documentation pages.

## 1.3.8 - 2024-04-15

#3440609 - Allow custom arguments to be passed to phpcs.\
#3440420 - Allow superuser to call composer on jobs.

## 1.3.7 - 2024-04-12

#3440448 - When requiring drush, do it with the the `-W` param.\
#3359927 - Document option to improve PHPUnit information in log.\
#3423154 - Change chromedriver for the new one used by core.

## 1.3.6 - 2024-04-10

#3426292 - Deprecate and rename some variables that were wrongly named.\
#3437131 - Remove drupal/core dependency in `composer.json` when running variants.

## 1.3.5 - 2024-04-08

#3432261 - ESLint on contrib is making incorrect recommendations.\
#3427357 - Move default phpcs configuration file and clean up unneeded lines in it.\
#3435899 - Only update documentation when merging into default branch.\
#3409296 - Add process for defining dynamic internal variables.\
#3400979 - Document additional options for PHPUnit deprecations.\
#3437578 - Add some missing extensions for PHP files.\
#3436889 - D7 Fix variable name used for max PHP version.

## 1.3.4 - 2024-04-03

#3375459 - Cater for version constraints in `DRUPAL_CORE` value.\
#3400979 - Document `SYMFONY_DEPRECATIONS_HELPER` options.\
#3436889 - D7 Allow modules to opt in to testing against max PHP version.\
#3437578 - PHPStan job fails while there is no PHP files to scan.\
#3437794 - Bump core stable to 10.2.5.\
#3436206 - Add lorem-ipsum spelling dictionary.

## 1.3.3 - 2024-04-02

#3437434 - Composer (next major) no longer works after 11.x updated to Symfony 7.\
#3437397 - Add missing phpstan (max PHP version) job.\
#3436819 - composer-lint fails if there are no php files in the project.\
#3431247 - Fix image references.\
#3422720 - D7 Test dependencies are not automatically brought.

## 1.3.2 - 2024-03-21

#3431247 - Add common customizations to the documentation site.\
#3432593 - Fix broken link in the contributing documentation.\
#3426136 - Execute CSpell in project root folder and make fixing words easier.\
#3432156 - Wrong references in phpstan and nightwatch next major jobs.

## 1.3.1 - 2024-03-19

#3431269 - Yarn install fails in "composer (next major)" job.\
#3419008 - Add logic to avoid running PHPUnit if the project has no PHPUnit tests.

## 1.3.0 - 2024-03-14

#3426647 - Check our own coding standards and basic editor configurations.\
#3426277 - Revert code changes and add alternative way via documentation pages.\
#3418831 - Test-only job.\
#3423402 - Document how to use the CSpell job.

## 1.2.3 - 2024-03-08

#3425959 - Remove `before_script` in "next major" jobs.\
#3426277 - Pass parallel options job to `run-tests.sh`.\
#3426392 - cspell: ignore commonly ignored paths and module machine name by default.\
#3423238 - Documentation site using GitLab pages.\
#3426104 - Bump D7 version.\
#3426013 - Bump core stable.\
#3397699 - Preserve original `composer.json` in the modules directory.\
#3425414 - Remove experimental flags.

## 1.2.2 - 2024-03-04

#3423236 - Unify symlink approach, allow folder argument.\
#3425414 - Remove experimental references in stable features.\
#3397699 - Preserve original `composer.json` in the modules directory.

## 1.2.1 - 2024-03-04

#3422323 - Improve CSpell, use pre-configured `.cspell.json`.\
#3421873 - Allow override of docker image tag.\
#3422720 - Test dependencies are not automatically brought on Drupal 7 modules.\
#3422978 - Update link in default template.\
#3422644 - Do not symlink the drush directory.\
#3422909 - Bump `CORE_NEXT_MINOR` after 10.3.x was released.

## 1.2.0 - 2024-02-16

#3405955 - Add job for CSpell in contrib pipeline.\
#3405813 - Create downloadable patch for ESLint automated fixes.\
#3419069 - Tidy up template and remove redundant lines.\
#3421674 - Add condition for mariadb.\
#3397270 - Nightwatch testing against all opted in versions.

## 1.1.2 - 2024-02-12

#3419698 - Make artifact names unique across jobs.\
#3419702 - Display version of tools used in each job.\
#3420374 - Cater for deprecating `SKIP_COMPOSER` in D7 pipelines.\
#3420529 - Allow extra options for stylelint job via variable.

## 1.1.1 - 2024-02-08

#3417987 - Amend ability to use a tag, even for files brought via curl.\
#3419178 - Increase `PHP_MAX` to 8.3 (part 1).\
#3414377 - Wrong PHPUnit's `junit.xml` location in Drupal 10.2.\
#3417807 - Being able to ignore CSS files.

## 1.1.0 - 2024-01-30

#3414391 - Changes:
- Redefined the rules to run the jobs, making them reusable and composable.
- A new variable called `OPT_IN_TEST_CURRENT` has been introduced to add the ability
to skip the current version for the test jobs.
- Marked `.skip-composer-rule` and `SKIP_COMPOSER` deprecated. Composer jobs
will run when they are needed by other jobs.

## 1.0.1 - 2024-01-18

#3415503 - Updated stable and security core versions as per latest releases.

## 1.0.0 - 2024-01-17

#3404175 - Initial stable release. Adopting semver labels.
