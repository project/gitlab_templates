# PHPStan

The `phpstan` jobs run static code quality checks using [PHPStan](https://phpstan.org/)

If the project contains a `phpstan.neon` configuration file it will be used. If not, a [default configuration](https://git.drupalcode.org/project/gitlab_templates/-/blob/main/assets/phpstan.neon) is used.

The pipeline variable `_PHPSTAN_LEVEL` can be set to a value from 0-9 to specify how relaxed or severe the code quality checks should be. This can be used to temporarily override the value in your `phpstan.neon` file. If no value is given, either in the variable or the file, then the default of 0 is used. See the [PHPStan rule levels](https://phpstan.org/user-guide/rule-levels) for more information.

Projects can optionally specify [baseline](https://phpstan.org/user-guide/baseline) file(s) of messages to ignore. The default baseline file `phpstan-baseline.neon` stored in the project top-level folder, will be included if using the default `phpstan.neon` config file. If your project's baseline file is called something different, then specify the name in the `_PHPSTAN_BASELINE_FILENAME` variable:
```
variables:
  _PHPSTAN_BASELINE_FILENAME: 'your-baseline-file.neon'
```

To include multiple baseline files, your project will need its own `phpstan.neon` config file with a `includes:` keyword, for example:
```
includes:
  - phpstan-baseline.neon
  - second-baseline-file.neon
```

A new baseline file is created which is available as a job artifact for download. This file will contain all warnings found, including those ignored in the single baseline file named in `_PHPSTAN_BASELINE_FILENAME`. Unmatched messages in the baseline file are reported in the log.

In addition to testing at the current core version, the `phpstan` job has optional variants for the next major and minor core versions and the maximum PHP version.

Consider the PHP version when interpreting the results of the PHPStan job, as the analysis may give different results between major versions of PHP.

If you do not want this job to be run, set the variable `SKIP_PHPSTAN: 1`
