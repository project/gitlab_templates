# Composer

The `composer` jobs are run automatically if a related job needs something from Composer. You cannot skip these jobs unless you skip PHPUnit and Nightwatch testing and also skip every validation/linting job that needs Composer.

## Updating dependencies when testing against future releases

When testing against future versions of Drupal, the minimum stability value is automatically set to ‘dev’, regardless of the value set in `composer.json`. Some dependencies may not be compatible with the target release, so they may need help to be enabled, using Lenient support and/or Composer patches.

### Lenient support

The variable `_LENIENT_ALLOW_LIST` can specify a comma-separated list of modules that Composer dependencies on the core version will be ignored for. This is achieved using the [mglaman/composer-drupal-lenient](https://github.com/mglaman/composer-drupal-lenient) Composer extension.

An example of this can be:
```
variables:
  OPT_IN_TEST_NEXT_MINOR: '1'
  OPT_IN_TEST_NEXT_MAJOR: '1'
  OPT_IN_TEST_MAX_PHP: '1'

composer (next minor):
  variables:
    _LENIENT_ALLOW_LIST: token

composer (next major):
  variables:
    _LENIENT_ALLOW_LIST: entity_reference_revisions,inline_entity_form,paragraphs,token
```

In this example, the lenient plugin will NOT be used in the regular Composer task. But it will be used for the next minor and next major with different lists of projects that are allowed, even if those projects are not compatible with the new release.


### Composer patches

Sometimes, you may need to install a patch for a dependent module to allow tests to pass.

To this, add a JSON file containing patches, and specify it in the `_COMPOSER_PATCHES_FILE` variable. For example:
```
composer (next major):
  variables:
    _LENIENT_ALLOW_LIST: token
    _COMPOSER_PATCHES_FILE: .gitlab-ci/nextmajor_patches.json
```

We recommend placing the patches within the `.gitlab-ci` folder inside your module.

Additional information on the patch file can be found in the [Composer Patches documentation](https://docs.cweagans.net/composer-patches//usage/defining-patches/#patches-file).

### Package versions

The templates will favor the `drupal/core-recommended` package when installing Drupal, which does NOT include a `composer.lock` file. This is expected and is the recommended usage for most cases.

If your module relies on specific versions of certain packages from core you need to update your module's `composer.json` to declare this.

The `composer` jobs in the templates set the expected Drupal core version, ignoring by default any possible core constraint set in the `composer.json` file. If you want this constraint to be considered, you need to do the following:
```
composer:
  variables:
    IGNORE_PROJECT_DRUPAL_CORE_VERSION: 0
```
