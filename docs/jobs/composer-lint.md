# Composer validation and PHP syntax check

The `composer-lint` job validates the project's `composer.json` file by executing the `composer validate` command.

It also uses the [php-parallel-lint](https://github.com/php-parallel-lint/PHP-Parallel-Lint) package to check the syntax of the module's PHP files. All files with an extension of `.php`, `.inc`, `.module`, and `.install` are checked.

If you need extra options for the command you can use the `_PARALLEL_LINT_EXTRA` variable. An example of this would be to add the `--show-deprecated` parameter which is off by default.
