# Stylelint

The `stylelint` job runs [Stylelint](https://stylelint.io/) checks on the CSS code of the project, using a [Drupal core configuration file](https://git.drupalcode.org/project/drupal/-/blob/11.x/core/.stylelintrc.json).

To disable a specific rule from reporting faults in a file, you can add a `stylelint-disable` comment to the file, for example:

`/* stylelint-disable order/properties-order */`

Files can be completely ignored by listing the paths in a `.stylelintignore` file in your project's top level directory. 

You can add extra options to the command using the `_STYLELINT_EXTRA` variable.
