# ESLint

The `eslint` job checks formatting standards in `.js` and `.yml` files.

[ESLint](https://eslint.org/) can be configured using an `.eslintrc.json` file that is located in your project's root directory. This file is optional and if it does not exist then a default configuration file with the same settings as `core/.eslintrc.passing.json` will be used. If you do create an `.eslintrc.json` file, it only needs to contain the specific rules that you want to override; all other rules will be inherited from the Core configuration because ESLint configuration cascades.

You can pass additional options to `eslint` via the `_ESLINT_EXTRA` variable. This variable has a default value of `--quiet` to match what Drupal core does.

If there are `.js` or `.yml` files that you do not want to fix, or if you cannot fix them for whatever reason, you can specify that the files should be ignored by adding the names to an `.eslintignore` file that is located in your project's root directory. Files and paths listed in `.eslintignore` will be ignored by both the ESLint and Prettier checks. For example, if your project contains minified javascript files these need to be ignored, so your `.eslintignore` should contain:
```
# Ignore all minified javascript files
**/*.min.js
```
### Extra Dependencies for CKEditor
If your project requires linting of `js` for `ckeditor`, and if the relevant dependencies are listed in a `package.json` file, then you can install those dependencies by adding the following to your `.gitlab-ci.yml`:
```
composer:
  after_script:
    - npm ci
```
You could use `npm install`, but if you have a `package-lock.json`, use `npm ci` instead because `npm install` may install dependencies that are more up-to-date than what is specified in `package-lock.json`, whereas `npm ci` will install exactly what is specified in `package-lock.json`.

## Prettier

[Prettier](https://prettier.io/) makes the formatting of both `.js` and `.yml` files "prettier", and is run as part of the `eslint` job.

Prettier's formatting rules follow the standards specified by Drupal Core as defined in `core/.prettierrc.json`. If you want to specify rules that differ from core's, you can do so in a `.prettierrc.json` file. But unlike ESLint, Prettier rules do not cascade, so if you include a `.prettierrc.json` file, you must specify all of the Core rules that you want your project to follow because they will not be inherited.

You can ignore files that you do not want to fix, or cannot fix, for Prettier formatting, by adding the file paths to a `.prettierignore` file. An example where this is needed is the Config and Views files exported from the UI, as these do not follow Prettier's formatting rules. So rather than manually editing the exported files, you can ignore them for Prettier processing (but still check them with ESLint).
```
config/*/views.*.yml
tests/modules/*/config/install/*.yml
```

## Fixing the reported problems

If there are standards and formatting errors that can be fixed automatically, you will get a useful artifact named `_eslint.patch` which you can run against your module's code to fix the issues that were found.
