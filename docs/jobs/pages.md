# GitLab Pages

The templates support the ability to publish a GitLab site. You can opt for using a supported default configuration or create your own integration.

## Default configuration

Projects can automatically publish a documentation site powered by [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

- Get started by creating `mkdocs.yml` and `docs/index.md` files.
- GitLab Pages has a [tutorial on how to write these files](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html).
- With those in place, a site gets re-published every time the default branch changes. We only auto-publish the default branch for now.
- [Material for MkDocs has a great docs site](https://squidfunk.github.io/mkdocs-material/) and offers lots of plugins that should work on our GitLab pages. You may need to install their prerequisites in a `before_script` step in the pages job.

### Project Logo
If the project has a logo image file `logo.png` this should be saved in the project's top-level directory, in accordance with the Drupal standard practice. However, the logo to be displayed in the MkDocs documentation site is taken from the `/docs` folder. This allows for an alternative logo on the documentation site. If there is no logo stored in the `/docs` folder then the file from the top-level folder will be used. You do not need to keep a duplicate file in both places.

## Non-default configuration

This option is also supported by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). All you need is to generate a `public` folder with the resulting static website that you want.

The job definition can be like this:
```
pages:
  stage: build
  image: ubuntu <==== Any base image that you feel comfortable with
  script:
    - apt get ... <==== Install needed dependencies
    - ./tool-of-choice . --output=public <==== Generate the site in the "public" folder.
    - echo "The documentation will be published at $CI_PAGES_URL"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

## PHPDocs Generators

There are different tools that can generate a full documentation site based on your project's code comments. Some examples for PHP projects could be:

- [Doctum](https://github.com/code-lts/doctum)
- [Phpdox](https://phpdox.net/)
- [PhpDocumentor](https://www.phpdoc.org/)

You could automate those tools and make them generate a folder containing the documentation site and then connect it with the previous code.
