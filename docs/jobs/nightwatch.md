# Nightwatch

If your module contains [Nightwatch](https://nightwatchjs.org/) tests, this job will run them.

It will set up a chrome browser and run the tests against it.

The results will be available as a `junit` artifact within the job.

This job allows for multiple variants to check previous, current and future versions of Drupal.

## Web driver versions

Tests on Drupal 11 and above will use Selenium and the latest Chrome W3C-compliant browser. Tests on Drupal 10 and below will continue to use the legacy Chrome driver which is not W3C-compliant. If your tests at Drupal 11 still need to use the legacy driver, add the following lines to a Nightwatch job to override the default behavior:

```
  # Set the services and web driver variables to the legacy versions.
  services:
    - !reference [ .with-database ]
    - !reference [ .with-chrome-legacy ]
  variables:
    DRUPAL_TEST_WEBDRIVER_HOSTNAME: $DRUPAL_TEST_WEBDRIVER_HOSTNAME_LEGACY
    DRUPAL_TEST_WEBDRIVER_PORT: $DRUPAL_TEST_WEBDRIVER_PORT_LEGACY
    DRUPAL_TEST_WEBDRIVER_W3C: "false"
```
