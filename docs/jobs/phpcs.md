# PHPCS

The `phpcs` job validates the project's source code against the [Drupal PHP coding standards](https://www.drupal.org/docs/develop/standards/php).

[PHP_CodeSniffer](https://github.com/PHPCSStandards/PHP_CodeSniffer/) is used to perform the checks that are configured in the Drupal standards as specified in the [Drupal Coder module](https://www.drupal.org/project/coder). The standards are curated by the [Drupal Coding Standards project](https://www.drupal.org/project/coding_standards).

## Customized configuration file
By default the full set of `Drupal` coding standards are checked, using this
[default phpcs.xml configuration file](https://git.drupalcode.org/project/gitlab_templates/-/blob/main/assets/phpcs.xml.dist). The `DrupalPractice` set of best practices are not enforced by default.

This job checks files with the following extensions: `.php`, `.inc`, `.module`, `.install`, `.info`, `.test`, `.profile`, `.theme`

A project may have its own `phpcs.xml` or `phpcs.xml.dist` configuration file which will be used instead of the default file.

The set of coding standards should be defined using the short path with just the name:
```
  <rule ref="Drupal"/>
  <rule ref="DrupalPractice"/>
```

## Custom options
You can use the pipeline variable `_PHPCS_EXTRA` in the `variables` section of the `.gitlab-ci.yml` file to pass additional arguments to the call. An example is ` -v ` to add verbose output to the log.
