# Upgrade status

The [Upgrade Status](https://www.drupal.org/project/upgrade_status) project helps you review the Drupal next major upgrade readiness of the environment and components of the module.

Use to prepare for a Drupal 10 to 11, 11 to 12 upgrade.

The module provides the following key features:

- Checks if you are using a version of Drupal that supports an upgrade.
- Checks if your system meets the next major version's system requirements.
- Integrates with the Update Status core module to inform you to update your contributed projects. Projects can be compatible with multiple major Drupal versions, so most projects can be updated on your existing site before doing the core major update.
- Runs PHPStan checks and a whole set of other checks to find any compatibility issues with the next Drupal major version that may remain (see more details below).
- Integrates with drush.

## Enabling this job

This experimental job is controlled by the variable `RUN_JOB_UPGRADE_STATUS` which is set to `0` by default. To run the job you need to set the value to `1` in your `.gitlab-ci.yml` file.
```
variables:
  RUN_JOB_UPGRADE_STATUS: '1'
```

## Arguments to the `analyze` call

You can pass additional arguments to the `drush upgrade_status:analyze` command by setting the `_UPGRADE_STATUS_ANALYZE_EXTRA` variable. 
```
variables:
  _UPGRADE_STATUS_ANALYZE_EXTRA: '--extra --options'
```

## How does this job compare with the "next major" job?

The "Upgrade status" job is an individual job as opposed to a full variant (composer + phpstan + phpunit + nightwatch). It can also help you with the fixes, as opposed to just showing the errors.