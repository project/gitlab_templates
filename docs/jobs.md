---
hide:
  - toc
---

# Jobs

We run multiple set up jobs (`composer`), lint jobs (`cspell`, `phpcs`...) and testing jobs (`phpunit`, `nightwatch`), and some of these also have the option to be run against previous or future Drupal versions.

Learn more about the different options regarding jobs below.

## SKIP and OPT_IN variables

You can control which jobs run or not by setting up a series of `SKIP_` and `OPT_IN_` variables.

You can skip:

- Any of the validation steps: `cspell`, `phpcs`, `phpstan`, `eslint`, `stylelint`.
- Any of the testing steps: `phpunit`, `nightwatch`, `test-only`.

The current available skip-variables are:

- `SKIP_COMPOSER_LINT`
- `SKIP_PHPCS`
- `SKIP_STYLELINT`
- `SKIP_ESLINT`
- `SKIP_CSPELL`
- `SKIP_PHPSTAN`
- `SKIP_NIGHTWATCH`
- `SKIP_PHPUNIT`
- `SKIP_TEST_ONLY_CHANGES`
- `SKIP_PAGES`

The _current_ version of all the above will be run by default, and you can skip on demand.

The templates also support testing not only the current version (on by default), but also the next minor and major, or previous minor and major.
All of these variants can be opted-in via the `OPT_IN_` variables.

Note that there are some time periods where certain variants would not make sense. For example, if Drupal 11 is the current variant, and the development for Drupal 12 has not started yet, then `OPT_IN_TEST_NEXT_MAJOR` will not run as there is no code to run it against. You do not need to change the value of this variable, as the job will start running again when it is appropriate to do so.

The current available opt-in variables are:

- `OPT_IN_TEST_CURRENT`
- `OPT_IN_TEST_MAX_PHP`
- `OPT_IN_TEST_PREVIOUS_MINOR`
- `OPT_IN_TEST_PREVIOUS_MAJOR`
- `OPT_IN_TEST_NEXT_MINOR`
- `OPT_IN_TEST_NEXT_MAJOR`

## RUN variables

These are similar to the SKIP variables and they are just `on/off` flags for jobs, using `1/0` respectively as the value.

The current available run-variables are:

- `RUN_JOB_UPGRADE_STATUS`


## When do the jobs run?

Each job will run or not run according to the following rules:

| JOB NAME                    | Conditions when it will run                             | Conditions when it will NOT run                           |
| --------------------------- | ------------------------------------------------------- | --------------------------------------------------------- |
| composer                    | SKIP_COMPOSER_LINT=0 \|\| SKIP_PHPCS=0 \|\|<br>SKIP_STYLELINT=0 \|\| SKIP_ESLINT=0 \|\|<br>SKIP_CSPELL=0 \|\| OPT_IN_TEST_CURRENT=1 | SKIP_COMPOSER_LINT=1 && SKIP_PHPCS=1 &&<br>SKIP_STYLELINT=1 && SKIP_ESLINT=1 &&<br>SKIP_CSPELL=1 && OPT_IN_TEST_CURRENT=0 |
| composer (max PHP version)  | OPT_IN_TEST_MAX_PHP=1                                   | OPT_IN_TEST_MAX_PHP=0                                     |
| composer (previous minor)   | OPT_IN_TEST_PREVIOUS_MINOR=1                            | OPT_IN_TEST_PREVIOUS_MINOR=0                              |
| composer (previous major)   | OPT_IN_TEST_PREVIOUS_MAJOR=1                            | OPT_IN_TEST_PREVIOUS_MAJOR=0                              |
| composer (next minor)       | OPT_IN_TEST_NEXT_MINOR=1                                | OPT_IN_TEST_NEXT_MINOR=0                                  |
| composer (next major)       | OPT_IN_TEST_NEXT_MAJOR=1                                | OPT_IN_TEST_NEXT_MAJOR=0                                  |
| composer-lint               | SKIP_COMPOSER_LINT=0                                    | SKIP_COMPOSER_LINT=1                                      |
| cspell                      | SKIP_CSPELL=0                                           | SKIP_CSPELL=1                                             |
| eslint                      | SKIP_ESLINT=0                                           | SKIP_ESLINT=1                                             |
| phpcs                       | SKIP_PHPCS=0                                            | SKIP_PHPCS=1                                              |
| stylelint                   | SKIP_STYLELINT=0                                        | SKIP_STYLELINT=1                                          |
| upgrade status              | RUN_JOB_UPGRADE_STATUS=1                                | RUN_JOB_UPGRADE_STATUS=0                                  |
| phpstan                     | SKIP_PHPSTAN=0 && OPT_IN_TEST_CURRENT=1                 | SKIP_PHPSTAN=1 \|\| OPT_IN_TEST_CURRENT=0                 |
| phpstan (max PHP version)   | SKIP_PHPSTAN=0 && OPT_IN_TEST_MAX_PHP=1                 | SKIP_PHPSTAN=1 \|\| OPT_IN_TEST_MAX_PHP=0                 |
| phpstan (next minor)        | SKIP_PHPSTAN=0 && OPT_IN_TEST_NEXT_MINOR=1              | SKIP_PHPSTAN=1 \|\| OPT_IN_TEST_NEXT_MINOR=0              |
| phpstan (next major)        | SKIP_PHPSTAN=0 && OPT_IN_TEST_NEXT_MAJOR=1              | SKIP_PHPSTAN=1 \|\| OPT_IN_TEST_NEXT_MAJOR=0              |
| nightwatch                  | SKIP_NIGHTWATCH=0 && OPT_IN_TEST_CURRENT=1              | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_CURRENT=0              |
| nightwatch (max PHP version)| SKIP_NIGHTWATCH=0 && OPT_IN_TEST_MAX_PHP=1              | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_MAX_PHP=0              |
| nightwatch (previous minor) | SKIP_NIGHTWATCH=0 && OPT_IN_TEST_PREVIOUS_MINOR=1       | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_PREVIOUS_MINOR=0       |
| nightwatch (previous major) | SKIP_NIGHTWATCH=0 && OPT_IN_TEST_PREVIOUS_MAJOR=1       | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_PREVIOUS_MAJOR=0       |
| nightwatch (next minor)     | SKIP_NIGHTWATCH=0 && OPT_IN_TEST_NEXT_MINOR=1           | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_NEXT_MINOR=0           |
| nightwatch (next major)     | SKIP_NIGHTWATCH=0 && OPT_IN_TEST_NEXT_MAJOR=1           | SKIP_NIGHTWATCH=1 \|\| OPT_IN_TEST_NEXT_MAJOR=0           |
| phpunit                     | SKIP_PHPUNIT=0 && OPT_IN_TEST_CURRENT=1                 | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_CURRENT=0                 |
| phpunit (max PHP version)   | SKIP_PHPUNIT=0 && OPT_IN_TEST_MAX_PHP=1                 | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_MAX_PHP=0                 |
| phpunit (previous minor)    | SKIP_PHPUNIT=0 && OPT_IN_TEST_PREVIOUS_MINOR=1          | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_PREVIOUS_MINOR=0          |
| phpunit (previous major)    | SKIP_PHPUNIT=0 && OPT_IN_TEST_PREVIOUS_MAJOR=1          | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_PREVIOUS_MAJOR=0          |
| phpunit (next minor)        | SKIP_PHPUNIT=0 && OPT_IN_TEST_NEXT_MINOR=1              | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_NEXT_MINOR=0              |
| phpunit (next major)        | SKIP_PHPUNIT=0 && OPT_IN_TEST_NEXT_MAJOR=1              | SKIP_PHPUNIT=1 \|\| OPT_IN_TEST_NEXT_MAJOR=0              |
| test-only changes           | SKIP_TEST_ONLY_CHANGES=0 && OPT_IN_TEST_CURRENT=1       | SKIP_TEST_ONLY_CHANGES=1 \|\| OPT_IN_TEST_CURRENT=0       |
| pages                       | SKIP_PAGES=0                                            | SKIP_PAGES=1                                              |
