---
hide:
  - toc
---

# GitLab Templates

The GitLab Templates project drives the GitLab CI integration for all Drupal contrib modules. The project page is located [here](https://www.drupal.org/project/gitlab_templates).

## Documentation

The [GitLab CI](https://www.drupal.org/docs/develop/git/using-gitlab-to-contribute-to-drupal/gitlab-ci) page offers an introduction to most of the topics.

This documentation site offers further insight into individual jobs, best practices, etc.

## Basic information

Look for further information on the following topics:

- [Set up](./info/setup.md)
- [Common tasks](./info/common.md)
- [Customizations](./info/customizations.md)
- [Variants](./info/variants.md)
- [Test Merge Requests](./info/testing-mrs.md)
- [Test locally](./info/test-locally.md)
- [Templates version](./info/templates-version.md)
- [Drupal 7](./info/drupal7.md)

## Jobs

[Jobs](./jobs.md): Learn about the different jobs, how to turn them on/off and the different variants available to test past and future versions of Drupal in your module.

Jump straight to the individual job's documentation page:

- [composer](./jobs/composer.md)
- [cspell](./jobs/cspell.md)
- [phpstan](./jobs/phpstan.md)
- [stylelint](./jobs/stylelint.md)
- [eslint](./jobs/eslint.md)
- [phpcs](./jobs/phpcs.md)
- [composer-lint](./jobs/composer-lint.md)
- [phpunit](./jobs/phpunit.md)
- [nightwatch](./jobs/nightwatch.md)
- [test-only](./jobs/test-only-changes.md)
- [pages](./jobs/pages.md)
- [upgrade status](./jobs/upgrade-status.md)
