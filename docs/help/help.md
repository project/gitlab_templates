# Help us

You can [contribute](./contributing.md) in the code or help with the [documentation](./documentation.md).

You can help by [submitting an issue](https://www.drupal.org/node/add/project-issue/gitlab_templates). Explain what the issue is and feel free to open a Merge Request with the suggested changes.

Use the `#gitlab` channel on [Drupal Slack](https://www.drupal.org/slack) or the issue queue to ask questions.
