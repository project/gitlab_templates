# Contributing to GitLab Templates

If you want to contribute to the GitLab templates, use the `main` branch as
reference, but work the [issue queue](https://www.drupal.org/project/issues/gitlab_templates?categories=All),
creating forks and merge requests so they can be tested and reviewed by others.

## Tags

**Branch name**: `main`

When new features or bugs are committed, we might want to create new tags.
We only have one branch (`main`), but we are following a semver approach to
tagging releases, so the community can decide whether they want a fixed
version, a recommended by the Drupal Association version, or the latest.

**Release tagging strategy**: X.Y.Z

We follow a linear development, but with tags so people can pin things:

- If only new bugs were merged from the last tag, we increase the Z. ie:
1.0.1, 1.0.2... 1.0.11
- If new features were added, we increase the Y. ie: from 1.0.11 to 1.1.0
- Once a new "minor" tag is created (ie: 1.1.0), we don't port anything to
1.0.11
- Breaking changes or a big refactoring increase X

For additional information on how contrib maintainers might use the above tags
see the [templates version](../info/templates-version.md) page.

**Maintainers only - semver tags and default tag**

There is a script to help with setting up the tags located at `scripts/do-git-tags.sh`.
Using this script will ensure that the desired tag is set and that also the
moving tags for minor and major updates are updated too.

There is a special tag called `default-ref` that is the value set in
`$_GITLAB_TEMPLATES_REF`. That tag can be set using the
`scripts/set-default-tag` script. It will point to the recommended tag for the
contrib space, so make sure that is a stable one.
