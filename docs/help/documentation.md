# Documentation

This documentation site has been put together by the community and it is a constant work in progress.

If you find a typo or want to help with documentation in general, you are welcome to open and issue and work on it.

## Run the site locally

To run the documentation site locally, run the following commands (you will need `docker`):
```
docker pull squidfunk/mkdocs-material
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
```

That will serve a site locally.

If you do not have `docker` in your system, you can get [MkDocs](https://www.mkdocs.org/getting-started/) and [MkDocs Material](https://squidfunk.github.io/mkdocs-material/getting-started/) installed, and then you can do `mkdocs serve .`.

## Check for spelling errors

You can check the site for spelling errors before committing with this commands:
```
npm i markdown-spellcheck -g
mdspell --en-us -n -a --report 'docs/**/*.md'
```
