# Customizations

## Overriding parts of the template

You can override any part of the template, like `variables`, `workflows` or individual job definitions.

## Variables

You can override any variable. The default values can be seen [here](https://git.drupalcode.org/project/gitlab_templates/-/blob/main/includes/include.drupalci.variables.yml).

```
variables:
  _SHOW_ENVIRONMENT_VARIABLES: "1"
  OPT_IN_TEST_NEXT_MINOR: 1
  SKIP_PHPSTAN: 1
```

Most variables are global in scope, as in the example above, where the the `variables:` keyword is at the top level. But some variables, such as `DRUPAL_CORE` and `PHP_VERSION` need to be scoped to a particular job, because the value has to change between the different [job variants](./variants.md). In this case, use the appropriate composer job name, for example:

```
composer:
  variables:
    DRUPAL_CORE: 10.3.6
```
or
```
composer (previous major):
  variables:
    PHP_VERSION: 8.1
```

Variables can be defined in the project's `.gitlab-ci.yml` as above, or in the pipeline UI form when setting scheduled pipelines or running a pipeline manually. Form fields will override the values defined in the file or derived in the gitlab templates processing. This [variable precedence hierarchy](https://docs.gitlab.com/ee/ci/variables/index.html#cicd-variable-precedence) page has the full details.

## Jobs

If you want to change the default behavior of some jobs, you just need to override that part of the job.

```
# Linting jobs are allowed to fail by default, let's change that below.
cspell:
  allow_failure: false
phpcs:
  allow_failure: false
phpstan:
  allow_failure: false
```

## Custom before_script and after_script

If your project needs to do extra processing in preparation before a job, or to do some custom work with results after a job, you can use the `before_script` and `after_script` job keywords. Every job can accept these customizations and they are specifically not used by Gitlab Templates, making them available for Contrib use. The variable `$DRUPAL_PROJECT_FOLDER` contains the absolute path to the contrib project within the Drupal site installation.

Examples:
```
# Remove files that you don't want to be checked.
phpcs:
  before_script:
    - rm $DRUPAL_PROJECT_FOLDER/some-file.inc

# Run a custom script after the job finishes.
phpstan:
  after_script:
    - php $DRUPAL_PROJECT_FOLDER/scripts/phpstan-baseline-summary.php
```

## References

If the part of the template that you are overriding uses in-template references, you don't need to replicate them in your overrides, you can just use the [!reference notation](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags).

For example: `!reference [ .setup-webserver ]`

## Additional services

You can include any other service and configure it as you would do it in GitLab CI.

For example, let's add a redis server to the `phpunit` job.
```
phpunit:
  services:
    - !reference [ .with-database ]
    - !reference [ .with-chrome ]
    - name: redis:6
```

## Additional extensions or packages

You can use `apt-get` to install new packages. For example, `imagemagick` needs extra packages:
```
.phpunit-base:
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends imagemagick
```

Or Redis needs the `redis` PHP extension:
```
.phpunit-base:
  before_script:
    - apt-get update
    - apt-get install -y --no-install-recommends $PHPIZE_DEPS
    - pecl install redis && docker-php-ext-enable redis
```

## Use your own base images

Installing many packages and other system changes can be slow. A separate image is faster, but needs to be maintained:
```
default:
  image:
    name: berdir/php-8.3-apache-extra:production
```

Where `berdir/php-8.3-apache-extra:production` Dockerfile would be something like this:
```
FROM drupalci/php-8.3-apache:production
RUN apt-get update && apt-get install -y ...
RUN pecl install redis && docker-php-ext-enable redis
```

## Project Name

The name of the project being tested is derived from the `*.info.yml` file in the top-level folder. If there is no top-level `*.info.yml` or there is more than one, then all `*.info.yml` files from the top-level and all sub-directories are considered, and the one with the shortest name is taken to be the main one. This is done because often sub-modules are named using the main module plus a suffix. If no `*.info.yml` files can be found in any directory then the project name is derived from the issue fork or repository name. If this process does not yield your required project name then you can set the value explicitly using:

```
variables:
  PROJECT_NAME: 'my_project'
```

## Workflow

The [workflow](https://docs.gitlab.com/ee/ci/yaml/workflow.html) of a repository defines when pipelines will be run. Various events, such as creating a Merge Request, committing to the default branch or submitting via the pipeline UI can be used in combinations of rules, and if any of the conditions are true then the pipeline will be run. Gitlab Templates defines a [default workflow](https://git.drupalcode.org/project/gitlab_templates/-/blob/main/includes/include.drupalci.workflows.yml) which in most cases will be exactly what your project needs. However, if you do need to extend this default workflow, you can use the following:

```
workflow:
  rules:
    - !reference [ .default-workflow ]
    - {add your extra rule here}
    - {and another}
```
Using this syntax avoids duplicating the entire default workflow, and ensures your project will get any updates or improvements made to the standard workflow.
