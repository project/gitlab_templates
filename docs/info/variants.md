# Variants (Drupal versions)

## Available variants

The templates support testing not only the current version of Drupal (on by default), but also the previous minor and major versions, and the next minor and major versions. We call these versions "variants" throughout the documentation.

All of these variants can be opted-in via the `OPT_IN_` variables.

The available opt-in variables are:

- `OPT_IN_TEST_CURRENT`
- `OPT_IN_TEST_MAX_PHP`
- `OPT_IN_TEST_PREVIOUS_MINOR`
- `OPT_IN_TEST_PREVIOUS_MAJOR`
- `OPT_IN_TEST_NEXT_MINOR`
- `OPT_IN_TEST_NEXT_MAJOR`

Note that there are some time periods where certain variants would not make sense. For example, if Drupal 11 is the current variant, and the development for Drupal 12 has not started yet, then `OPT_IN_TEST_NEXT_MAJOR` will not run as there is no code to run it against. You do not need to change the value of this variable, as the job will start running again when it is appropriate to do so.

## Useful variables and how they connect to existing variants

These variables are the ones used in the different variants:

- `CORE_STABLE` is used for `OPT_IN_TEST_CURRENT` (the default variant) and for `OPT_IN_TEST_MAX_PHP`.

- `CORE_SECURITY_PREVIOUS_MINOR` is used for `OPT_IN_TEST_PREVIOUS_MINOR`.

- `CORE_PREVIOUS_STABLE` is used for `OPT_IN_TEST_PREVIOUS_MAJOR`.

- `CORE_NEXT_MINOR`	is used for `OPT_IN_TEST_NEXT_MINOR`.

- `CORE_MAJOR_DEVELOPMENT` is used for `OPT_IN_TEST_NEXT_MAJOR`.

- `CORE_LEG_STABLE` is the default value for `_TARGET_D7_CORE` and is used for Drupal 7 testing.

If you want to alter how variants behave and which versions they run against, you can use the `variables` section of your `.gitlab-ci.yml` file to set the desired values for those variables.

## Create new variants

Should you need to create new variants, you could do it by copying some code and adapting it. If we look at the `...(previous major)` variant, we see these blocks:
```
composer (previous major):
  extends: .composer-base
  rules:
    - *opt-in-previous-major-rule
    - when: always
  variables:
    PHP_VERSION: $CORE_PREVIOUS_PHP_MIN
    DRUPAL_CORE: $CORE_PREVIOUS_STABLE
    IGNORE_PROJECT_DRUPAL_CORE_VERSION: 1

nightwatch (previous major):
  extends: nightwatch
  rules:
    - *opt-in-previous-major-rule
    - *skip-nightwatch-rule
    - *nightwatch-tests-exist-rule
  needs:
    - "composer (previous major)"

phpunit (previous major):
  extends: phpunit
  rules:
    - *opt-in-previous-major-rule
    - *skip-phpunit-rule
    - *phpunit-tests-exist-rule
  needs:
    - "composer (previous major)"
```

If you want to add a new variant to your project, you can extend the existing jobs, and just alter the necessary versions. Here is an example for testing with Drupal 9:
```
composer (D9):
  extends: .composer-base
  variables:
    PHP_VERSION: "8.2"
    PHP_IMAGE_VARIANT: "apache"
    DRUPAL_CORE: "9.5.2"
    IGNORE_PROJECT_DRUPAL_CORE_VERSION: 1

nightwatch (D9):
  extends: nightwatch
  needs:
    - "composer (D9)"

phpunit (D9):
  extends: phpunit
  needs:
    - "composer (D9)"
```

If your module does not have Nightwatch or PHPUnit tests, you can remove that particular block.

Code validation, coding standards and linting jobs are run against the "current" core version, so you do not need to make duplicate variants for those jobs.
