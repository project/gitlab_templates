# Testing Gitlab Templates Merge Requests

## Specifying a Merge Request via form variables

If a project wants to test using a branch/MR of this repository, go to your project's CI/CD settings page and add the variables below. All pipelines in the project will use the specified repository+branch until the variables are removed.

- KEY = `_GITLAB_TEMPLATES_REPO` VALUE = `issue/gitlab_templates-nnnnnnn`
- KEY = `_GITLAB_TEMPLATES_REF` VALUE = `branch name`, often the format is `nnnnnnn-some-text`

The variables should have the following attributes:

- Type = Variable (default)
- Environment Scope = All (default)
- Protected = No
- Masked = No
- Expanded = Yes

This method of testing will only work if you have the unchanged default settings for `project` and `ref` in your `.gitlab-ci.yml`, as shown here:
```
  - project: $_GITLAB_TEMPLATES_REPO
    ref: $_GITLAB_TEMPLATES_REF
```

## Specifying a Merge Request in the .gitlab-ci.yml file

You can also test a Merge Request by changing these two lines in your `.gitlab-ci.yml` file:
```
  - project: $_GITLAB_TEMPLATES_REPO
    ref: $_GITLAB_TEMPLATES_REF
```
to
```
  - project: issue/gitlab_templates-12345
    ref: 12345-branch-name
```
where `project` is set to the fork name (`issue/gitlab_templates-12345`) and `ref` is set to the branch name (`12345-branch-name`).

If the MR is testing assets provided by the fork (like modifications to `.cspell.json`, `phpcs.xml.dist` or `symlink_project.php`), you wil also need to set two additional variables `_CURL_TEMPLATES_REPO` and `_CURL_TEMPLATES_REF` to match the `project` and `ref` values:
```
variables:
  _CURL_TEMPLATES_REPO: issue/gitlab_templates-12345
  _CURL_TEMPLATES_REF: 12345-branch-name
```
