# Templates behavior in contrib modules

If you want to change the release or version of GitLab Templates that is used for the CI pipelines, find the following lines in your `.gitlab-ci.yml` file:
```
include:
  - project: $_GITLAB_TEMPLATES_REPO
    ref: $_GITLAB_TEMPLATES_REF
```

The value of `project` should not be changed, but you can change `ref`

- `ref: $_GITLAB_TEMPLATES_REF` - This is the recommended (and default) value.
The Drupal Association will update this value periodically to the recommended tag. This will include new features and bug fixes after a short delay, to ensure stability. Internally the value of this tag is `default-ref`
- `ref: main` - Get the very latest additions and bug fixes as soon as they are merged into the templates. There is no delay here, you get the all changes immediately, so you need to accept that there may be problems.
- `ref: 1.2.0` - a fixed known tag value. This will not move and not get any updates. It is up to the maintainer to change this when necessary.

There are also some special _moving_ tags. With these you can get the latest changes but without jumping a minor or major version. These tags are suffixed with `-latest` and can be checked in [this tags listing](https://git.drupalcode.org/project/gitlab_templates/-/tags).

For example:

- `ref: 1.0.x-latest` - moving tag for the latest bug fixes. It will always point to the latest existing `1.0.number` release but not jump to the next minor (which would be 1.1.0 in this example). This mimics the `~1.0` semver constraint.
- `ref: 1.x-latest` - moving tag for the latest minor features. It will always point to the latest existing `1.minor.number` release, but not jump to the next major version (which would be `2.0.0` in this example). This mimics the `~1` semver constraint.

Note that `main` is not a tag, that is just the name of the default branch, so it will always contains the latest commits.

If you set any value for `ref` other than the default `$_GITLAB_TEMPLATES_REF` then you also need to create a variable named `_CURL_TEMPLATES_REF` and set it to the same value. For example
```
variables:
  _CURL_TEMPLATES_REF: main
```

## What commits are in which versions?
There may be times when you want to verify which commits are contained in a particular release or what the current state is. Here are some useful links:

- What commits are in the `default` release? [Commits](https://git.drupalcode.org/project/gitlab_templates/-/commits/default-ref?ref_type=tags)

- What commits are in `main` but not yet in the `default` release? [Commits](https://git.drupalcode.org/project/gitlab_templates/-/compare/default-ref...main?from_project_id=96292)

- Here are the commits that are [not yet in the `1.x-latest` release](https://git.drupalcode.org/project/gitlab_templates/-/compare/1.x-latest...main)

