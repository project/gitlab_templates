# Drupal 7

To run gitlab pipelines in a Drupal 7 project, create a `.gitlab-ci.yml` file from the standard template as explained [here](./setup.md). Then change one line from this:

```
- '/includes/include.drupalci.main.yml'
```

to this:

```
- '/includes/include.drupalci.main-d7.yml'
```

## Drupal 7 core version

By default, the latest stable Drupal 7 core version is used, but you can change this by specifying a value for the `_TARGET_D7_CORE` variable:
```
variables:
  _TARGET_D7_CORE: 7.98
```

## Composer

If your Drupal 7 module uses `drupal/composer` and has a `composer.json` file, you'll need to set the `_D7_DRUPAL_COMPOSER_NEEDED` variable to 1.
```
variables:
  _D7_DRUPAL_COMPOSER_NEEDED: 1
```
This will use Composer 1 instead of 2, but it should allow your module to work as expected and bring in the requirements using Composer.

## Dependencies

If your module has dependencies and you are _not_ managing them with a `composer.json` file, you can use the `_D7_DRUPAL_TEST_DEPENDENCIES` variable with a list of modules. For example:
```
variables:
  _D7_DRUPAL_TEST_DEPENDENCIES: "entity field_collection link"
```
These modules will be downloaded via `drush pm:download`. You can specify different variations in the list of modules:

- Dev version of a module: `entity -dev`
- Exact version of a module: `entity-7.x-1.3`

Some modules might not be hosted in `git.drupalcode.org`. If you need to provide more complex arguments when downloading a module or dependency, you can use the `before_script` section and then specify arguments like `--source`. You must set the `--destination` parameter so the module is saved in the right location.

Example:
```
phpunit:
  before_script:
    - vendor/bin/drush dl devel --source=https://gitlab.com/d7security/d7security/-/raw/main --destination=$_WEB_ROOT/sites/all/modules/
```
