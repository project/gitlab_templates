# Set up

Your first step is to add a `.gitlab-ci.yml` file to the root of your project.

The Drupal Association and the community have provided everything you need in a template to make it easy to get started. By using the Drupal Association `.gitlab-ci.yml` template, your project will be configured to run the most commonly needed test scenarios for you. This template also 'includes' supplementary configuration which can be updated by the Drupal Association on an ongoing basis. This means we can update 'variables' keywords to keep your testing up to date with the latest versions of Drupal and supported environments, and you do not need to make any changes to stay up to date!

The template is configured to work by default with contrib modules for newer versions of Drupal (9 and above), so note that if you are adding this to your Drupal 7 contrib module, you will need to comment out one line and uncomment another. Everything is explained in the "`.gitlab-ci.yml`" file.

## Using the Drupal Association .gitlab-ci.yml template

The easiest way to do this is through the GitLab UI.

If you are a maintainer, you can do this directly against the repository. But if you are a contributor, or even as a maintainer too, we recommend creating an issue in the project, open a fork and a Merge Request, and do the following:

- From the fork page, navigate to the source code on `git.drupalcode.org/issue/fork-name/fork-branch-used-in-mr`.
- Then use the repository UI (not the Web IDE) to add a new file.
- Add a new file and type as file name `.gitlab-ci.yml`.
- You will be presented with the option to select a template, select the Drupal Association's `template.gitlab-ci.yml` from the top of the list.

![Add GitLab CI default file](/gitlab_templates/assets/add-gitlab-ci.png)

- Commit that file.

You can also do all the above manually via `git`.

- Checkout your project or fork.
- Create a new file called `.gitlab-ci.yml` in the root of the project.
- Copy in the contents of the [template](https://git.drupalcode.org/project/gitlab_templates/-/blob/main/gitlab-ci/template.gitlab-ci.yml).
- Commit and push your changes.

## Confirm that it works

When you first commit your `.gitlab-ci.yml` file the pipeline should be triggered automatically, either in the branch (if you are committing straight to it) or in the Merge Request (if you are doing the integration via an issue).

You should see something like this:

![Default pipeline](/gitlab_templates/assets/default-pipeline.png)

## When will pipelines run?

The default configuration from the provided template will run jobs when:

- There is a merge request event
- There is a commit tag
- When an upstream pipeline calls it
- There is a commit to the default branch
- If triggered manually from the 'Run pipelines' function
- If triggered manually from the Web IDE

## Using your own .gitlab-ci.yml file

If you would prefer to use your own `.gitlab-ci.yml` file, or to modify the template, you are certainly welcome to do so. GitLab provides extensive [documentation for GitLab CI](https://docs.gitlab.com/ee/ci/), or you can join the `#gitLab` channel in [Drupal Slack](https://www.drupal.org/join-slack).

## Using the templates in another instance

It is possible to use the templates in a different instance, all you need to do is change the `include` lines from a `local` notation to a `remote` notation, like this:
```
include:
  - remote: https://git.drupalcode.org/${_GITLAB_TEMPLATES_REPO}/-/raw/${_GITLAB_TEMPLATES_REF}/includes/include.drupalci.main.yml
  - remote: https://git.drupalcode.org/${_GITLAB_TEMPLATES_REPO}/-/raw/${_GITLAB_TEMPLATES_REF}/includes/include.drupalci.variables.yml
  - remote: https://git.drupalcode.org/${_GITLAB_TEMPLATES_REPO}/-/raw/${_GITLAB_TEMPLATES_REF}/includes/include.drupalci.hidden-variables.yml
  - remote: https://git.drupalcode.org/${_GITLAB_TEMPLATES_REPO}/-/raw/${_GITLAB_TEMPLATES_REF}/includes/include.drupalci.workflows.yml
```

And note that `_GITLAB_TEMPLATES_REPO` and `_GITLAB_TEMPLATES_REF` would need to be defined in your instance too. If you do not want to define these variables you can also hardcode the values to be `project/gitlab_templates` and `default-ref` (or your preferred reference). For example:
```
include:
  - remote: https://git.drupalcode.org/project/gitlab_templates/-/raw/default-ref/includes/include.drupalci.main.yml
  ...
```
