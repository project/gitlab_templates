# Common tasks

## Scheduling pipelines

If you would like to schedule pipelines to run on automatically, separately from the triggers defined by the template above, you can do so with [Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

- Navigate to your project on Drupal GitLab
- Navigate to Build -> Pipelines schedules in the sidebar.
- Click New Schedule button.

From here, you can set an interval pattern, such as every day, week, or month, or a custom pattern based on standard cron syntax. You can then choose a target branch or tag, and set any variable values.

![Scheduled pipelines](/gitlab_templates/assets/pipeline-schedule.png)

## Gathering results

There are multiple ways to gather GitLab CI results, depending on what data you need.

- You can use the test result summaries from the pipeline, which will highlight test failures and summarize successes.
- You can download the test artifacts.
- You can view the full console output of any particular job by clicking through it.

If you need additional debugging, you can also leverage GitLab CI features to capture additional logs from some of the services that are running, by setting `CI_DEBUG_SERVICES` variable to `"true"`. You can read more about this feature on the GitLab documentation page [here](https://docs.gitlab.com/ee/ci/services/#capturing-service-container-logs). We only recommend this while debugging, as the volume and size of artifacts can grow very rapidly with this feature enabled.

## Composer dependencies and configuration

Everything in the module's `composer.json` is merged into project `composer.json`.

Alternatively, you can also use the `before_script` with the `--no-update` flag to require additional modules.
```
.composer-base:
  before_script:
    - composer require --dev predis/predis --no-update
```

## Including a non-required module for testing

Add the module to the `require-dev` section of your module's `composer.json` file. It will be installed only when requested by developers and the CI process.

## Install Javascript libraries

If packages cannot be installed via `composer` or `yarn`, you can still download them directly.

For example, DropzoneJS needs the Dropzone JS library:
```
variables:
  DROPZONE_VERSION: v5.9.3

.composer-base:
  after_script:
    - mkdir $_WEB_ROOT/libraries
    - cd $_WEB_ROOT/libraries
    - curl -L -o dropzone.zip --silent
      https://github.com/dropzone/dropzone/releases/download/$DROPZONE_VERSION/dist.zip
    - unzip dropzone.zip
    - mv dist dropzone
```

## Test multiple database combinations

You can test multiple database types and versions leveraging the `parallel:matrix` feature. You can see an example in the `date_point` module.

Remember that core already tests the different database types and versions, so this is only recommended if your module does some operations that can be database-specific and you need to support multiple database drivers (check the allowed images in [drupalci_environments](https://git.drupalcode.org/project/drupalci_environments/-/tree/production/db?ref_type=heads) project). It can be as easy as doing this in the template:

```
phpunit:
  parallel:
    matrix:
      - _TARGET_DB_TYPE: 'mysql'
        _TARGET_DB_VERSION: '8'
      - _TARGET_DB_TYPE: 'pgsql'
        _TARGET_DB_VERSION: '16'
```

Another example could be testing different extensions if a module allows for that. For example, Redis tests run with different extensions:
```
phpunit:
  parallel:
    matrix:
      - REDIS_INTERFACE:
          - PhpRedis
          - Predis
          - Relay
```

## Concurrent test runs

Projects with more than a few tests can use core's `run-tests.sh` with concurrency feature.

```
variables:
  _PHPUNIT_CONCURRENT: '1'
```

## Test different Drupal core and PHP versions

To specify which versions of Drupal Core and PHP are used for running tests, the following "opt_in" variables can be set to 1 or 0. The default values are:

```
variables:
  OPT_IN_TEST_CURRENT: 1
  OPT_IN_TEST_PREVIOUS_MAJOR: 0
  OPT_IN_TEST_PREVIOUS_MINOR: 0
  OPT_IN_TEST_NEXT_MINOR: 0
  OPT_IN_TEST_NEXT_MAJOR: 0
  OPT_IN_TEST_MAX_PHP: 0
```
Use only what makes sense for your project. Pipelines for Drupal 9+ make use of all these six variables. Testing with Drupal 7 only uses `OPT_IN_TEST_CURRENT` and `OPT_IN_TEST_MAX_PHP`

You can combine the above with manual execution (to save resources and trigger only when needed) by doing something like this:
```
phpunit (next major):
  rules:
    - !reference [ .opt-in-next-major-rule ]
    - !reference [ .skip-phpunit-rule ]
    # Do not automatically run, but allow the job to be started manually.
    # To do this the .phpunit-tests-exist-rule is removed from here.
    - when: manual
```

## Test different SQLite versions

The default images ship with SQLite 3.45, and the default image variant is `ubuntu-apache`. If you want to test with older Core and PHP versions, you can set the `PHP_IMAGE_VARIANT` to `apache` in the specific composer variant, for example:
```
composer (drupal 9):
  variables:
    PHP_IMAGE_VARIANT: 'apache'
```
Note that not all PHP versions have a `apache` and `ubuntu-apache` variation. You can check the available PHP images in the [drupalci_environments](https://git.drupalcode.org/project/drupalci_environments/-/tree/dev/php) project.

## Viewing environment variables

During development or when debugging a job, you may want to view all the environment variables created in the pipeline. To do this, add the following to your .gitlab-ci.yml file
```
variables:
  _SHOW_ENVIRONMENT_VARIABLES: "1"
```
 You can also set this variable in the 'run pipeline' form. The information is displayed in the `composer` and `phpunit` job log, inside an expandable drop-down which is closed by default. If you want the environment variables to be displayed in other jobs, add the following reference into a `before_script` or `after_script` section
```
  - !reference [ .show-environment-variables ]
```
Most environment variables are created when the pipeline is first built, but there are a small number which are added in the `composer` job (and its variants, if these are enabled). These are automatically available in every subsequent job. However, in the rare scenario that you need one of these custom variables in your `composer` job(s), add the following reference to create the variables in the appropriate `before_script` or `after_script` section
```
  - !reference [ .create-environment-variables ]
```

## Adding a pipeline status badge

Gitlab provides automatic images of the status of pipelines. You can add this to your project's readme.md file to give a quick visual indication of the state when browsing the repository.
The details are on [GitLab badges](https://docs.gitlab.com/ee/user/project/badges.html).

## Running pipelines via the GitLab UI

You can run pipelines from the GitLab UI by navigating to: `Build` > `Pipelines` > `Run pipeline`. When you do this, all the variables inherited from the templates will be presented in the form and you can override any value. These changes will take priority over any pre-defined value or value set in the `.gitlab-ci.yml` file.

## Creating new jobs that require database and browser services

If you need to create new jobs that require the database and browser to be wired up, you just need to extend from a predefined base job:
```
your-new-job:
  extends: .testing-job-base
```

This will connect the `database` and `chrome` services, in the same way that is done for the `phpunit` and `nightwatch` jobs. You still need to set up the rest of the job elements.

You can easily install Drupal inside the job like this: `php $_WEB_ROOT/core/scripts/drupal install standard`.

A full example could look like the below code:
```
drush-status:
  extends: .testing-job-base
  needs:
    - composer
  script:
    # Setup the webserver and calculate the database connection information (if needed).
    - !reference [ .setup-webserver ]
    - !reference [ .simpletest-db ]
    - php $_WEB_ROOT/core/scripts/drupal install standard
    # This line could go into the module's composer.json file. It's here for demonstration purposes.
    - composer require drush/drush
    - vendor/bin/drush --root=$_WEB_ROOT st
    # Do something else...
```
