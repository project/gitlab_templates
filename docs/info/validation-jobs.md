# Validation jobs

There are several code validation jobs, and these attempt to mirror what Drupal Core does. Separate documentation for each of these jobs is in:

- [composer-lint](../jobs/composer-lint.md)
- [cspell](../jobs/cspell.md)
- [eslint](../jobs/eslint.md)
- [phpcs](../jobs/phpcs.md)
- [phpstan](../jobs/phpstan.md)
- [stylelint](../jobs/stylelint.md)

## Allow failure in pipelines

By default, all validation jobs are allowed to fail. This means that even if a job reports failures, the overall result of the pipeline will not be affected by it. If there is a failure, the job will display an amber 'warning' instead of a red 'fail'.

If a job is not allowed to fail then any reported problems will produce  a red 'fail' and the overall pipeline status will be 'failed'.

There are several ways to alter this behavior, listed below.

### All validation jobs
A variable named `_ALL_VALIDATE_ALLOW_FAILURE` can be used to control all the validation jobs `allow_failure`-behavior at once.

Set this to 1 to allow the all the validation/linting jobs to fail, or set it to 0 for not allowing any failure. The default value is blank, which means that the pipeline jobs `allow_failure:` setting will be respected.

Setting this variable in a pipeline UI form will override the setting committed in the project's `.gitlab-ci.yml` file.
```
variables:
  _ALL_VALIDATE_ALLOW_FAILURE: 0
```

### Overriding the 'all' setting per job
There are individual variables to control the `allow_failure` setting for each job. These variables will take precedence over the `_ALL_VALIDATE_ALLOW_FAILURE` setting. Set to 1 to allow failure or 0 for not allowing failure. Leave blank or do not include the variable name to use the 'all' setting.
```
variables:
  _COMPOSER_LINT_ALLOW_FAILURE: 0
  _CSPELL_ALLOW_FAILURE: 0
  _ESLINT_ALLOW_FAILURE: 0
  _PHPCS_ALLOW_FAILURE: 0
  _PHPSTAN_ALLOW_FAILURE: 0
  _STYLELINT_ALLOW_FAILURE: 0
```

### Override the job `allow_failure` value directly
You can also override the job value definition in your `.gitlab-ci.yml` file for the linting jobs where you want to change this behavior from the Gitlab Templates default.
```
cspell:
  allow_failure: false
```

## Order of precedence
The `allow_failure` setting for a particular job is determined by the following order - the first match in this list takes priority:

1. The job-specific `_{name}_ALLOW_FAILURE` variable set in the UI pipeline form.
1. The job-specific variable as defined in the `.gitlab-ci.yml` file
1. `_ALL_VALIDATE_ALLOW_FAILURE` via the UI form
1. `_ALL_VALIDATE_ALLOW_FAILURE` in the `.gitlab-ci.yml`
1. The `allow_failure:` keyword setting for the job in `.gitlab-ci.yml`
1. The `allow_failure:` keyword in the Gitlab Templates pipeline default.
