# Running GitLab CI tests locally

## ddev-drupal-contrib

DDEV users working on contrib modules are encouraged to use the [ddev-drupal-contrib service](https://github.com/ddev/ddev-drupal-contrib). This assembles a codebase and provides Composer scripts that mimic GitLab CI. See the repository README for detailed installation and usage instructions.

## Running GitLab CI containers locally

An alternative is to run our GitLab containers and tests locally. Unfortunately, the local runner from GitLab itself has been deprecated and [doesn't support a lot of features](https://docs.gitlab.com/runner/commands/#limitations-of-gitlab-runner-exec). But there is a package available on GitHub, [firecow/gitlab-ci-local](https://github.com/firecow/gitlab-ci-local), which supports most features.

### Installation

The easiest way to install the tool is to use npm: `npm install -g gitlab-ci-local`. You can also install the tool with apt on Debian-based systems or use Homebrew to install on MacOS.

You also need to have Docker available, the best way to run docker depends on your environment.

The package requires a few variables to be set so it understands the Drupal GitLab templates. To make it easier to run commands, use a bash script or an alias.

For example, put this code in `/usr/local/bin/drupal-ci-local`

```
#!/bin/bash
gitlab-ci-local \
  --remote-variables git@git.drupal.org:project/gitlab_templates=includes/include.drupalci.variables.yml=main \
  --variable="_GITLAB_TEMPLATES_REPO=project/gitlab_templates" \
  "$@"
```

or as an alternative create an alias in your shell:
```
alias drupal-ci-local='gitlab-ci-local --remote-variables git@git.drupal.org:project/gitlab_templates=includes/include.drupalci.variables.yml=main --variable="_GITLAB_TEMPLATES_REPO=project/gitlab_templates"'
```

This way you can just call `drupal-ci-local` in your shell and run GitLab CI tests or test your `.gitlab.ci.yml`.

### Running tests

Now that you have the tooling installed, you can run the tests locally. To test if everything works. So for example:

```
$ git clone git@git.drupal.org:project/keycdn
$ cd keycdn
$ drupal-ci-local composer # runs the composer step
$ drupal-ci-local # runs all steps!

Warning: cloning from https:// urls does not work completely without issues.
```

All the changes that GitLab makes also are made to your project. So should you run into something really weird, you can actually debug it!

This behavior can also be adjusted by using `--shell-isolation`. This option also makes sure artifact are handled correctly.

If you want to inspect the templates, artifact or anything there is a directory in your project called `.gitlab-ci-local`. There you will find an artifacts directory. But that directory also contains the included templates and some other goodies.

### Chromedriver tests issues

There are still some quirks, when using chromedriver the configuration needs some changes to know where to go. There are multiple containers:

- The main container is called: “build”
- The database is called: “database”
- The chromedriver is called: “chrome”

The current implementation cheats. It makes all services available on localhost on the runner. This is not default behavior and uses a feature flag. The proper implementation would be to use the container names to get things up and running. This should be possible, but will need some work to make sure testing URLs and chromedriver URLs are correct.

### Read the manual, there is more to discover

Read the readme on the repository: [firecow/gitlab-ci-local](https://github.com/firecow/gitlab-ci-local). There are loads of other things you can do like global and local variables, use a .env file for settings, add bash completion, list jobs, and more.
