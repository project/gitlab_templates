#!/bin/bash

# Script to revert all changed files that are not tests.
# Drupal 7 version.

TARGET_BRANCH=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}${CI_COMMIT_BRANCH}
BASELINE=${CI_MERGE_REQUEST_TARGET_BRANCH_SHA:-$CI_MERGE_REQUEST_DIFF_BASE_SHA}
git config --global --add safe.directory $CI_PROJECT_DIR

echo "ℹ️ Changes from ${TARGET_BRANCH} branch (SHA $BASELINE)"
git diff ${BASELINE} --stat=120,110
echo "If this list contains more files than you changed, you may need to rebase your branch."
git status

echo "1️⃣ Reverting changes that are not test files"
# In addition to the test files, do not revert changes to composer.json, .info and phpunit.xml
PATTERN='(\.test$|tests/|composer\.json|\.info|phpunit\.xml|\.gitlab-ci\.yml)'
CHANGES_TO_REVERT=$(git diff ${BASELINE} --diff-filter=DM --name-only | grep -Ev $PATTERN) || true
if [ "$CHANGES_TO_REVERT" != "" ]; then
  for file in $CHANGES_TO_REVERT; do
    echo "↩️ Reverting $file";
    git checkout ${BASELINE} -- $file;
  done
fi

ADDED_FILES=$(git diff ${BASELINE} --diff-filter=A --name-only | grep -Ev $PATTERN) || true
if [ "$ADDED_FILES" != "" ]; then
  for file in $ADDED_FILES; do
    echo "🗑️️ Deleting $file";
    git rm $file;
  done
fi

printf "$DIVIDER\nAfter reverting all non-test changes, the remaining changed files are:\n"
git diff ${BASELINE} --stat=120,110

TESTS_CHANGED=$(git diff ${BASELINE} --name-only | grep -E '\.test$') || true
if [ "$TESTS_CHANGED" != "" ]; then
  printf " \n2️⃣ Running tests changed in this merge request\n \nThe following test files have been changed, and only these tests will be run:\n$TESTS_CHANGED\n "
  for file in $TESTS_CHANGED; do
    test=$DRUPAL_PROJECT_FOLDER/$file
    printf "$DIVIDER\nexecuting: sudo SYMFONY_DEPRECATIONS_HELPER='$SYMFONY_DEPRECATIONS_HELPER' MINK_DRIVER_ARGS_WEBDRIVER='$MINK_DRIVER_ARGS_WEBDRIVER' -u www-data php $_WEB_ROOT/scripts/run-tests.sh --color --concurrency '32' --url $SIMPLETEST_BASE_URL --verbose --fail-only --xml $BROWSERTEST_OUTPUT_DIRECTORY --file $test $_PHPUNIT_EXTRA\n "
    sudo SYMFONY_DEPRECATIONS_HELPER="$SYMFONY_DEPRECATIONS_HELPER" MINK_DRIVER_ARGS_WEBDRIVER="$MINK_DRIVER_ARGS_WEBDRIVER" -u www-data php $_WEB_ROOT/scripts/run-tests.sh --color --concurrency "32" --url $SIMPLETEST_BASE_URL --verbose --fail-only --xml $BROWSERTEST_OUTPUT_DIRECTORY --file $test $_PHPUNIT_EXTRA || EXIT_CODE=$?
  done;
else
  printf "$DIVIDER\n2️⃣ No test files were changed. The 'test-only changes' job can do nothing.$DIVIDER\n"
  EXIT_CODE=0
fi

cp /var/log/apache2/test.apache.access.log $CI_PROJECT_DIR/apache.access.log.txt
cp /var/log/apache2/test.apache.error.log $CI_PROJECT_DIR/apache.error.log.txt

echo "Exiting with EXIT_CODE=$EXIT_CODE"
exit $EXIT_CODE
