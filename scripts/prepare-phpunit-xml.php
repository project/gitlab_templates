#!/usr/bin/env php
<?php

/**
 * @file
 * Process the core phpunit.xml to make it suitable for contrib testing.
 *
 * Arguments:
 *   -v --verbose         Show verbose debug output.
 *   -s --suffix {string} Additional suffix to append to the input filename
 *                        before writing out, when testing the script locally,
 *                        to avoid overwriting the original file.
 */

// Get the options.
$options = getopt('s:v', ['suffix:', 'verbose']);
$quiet = !array_key_exists('v', $options) && !array_key_exists('verbose', $options);
$suffix = $options['s'] ?? $options['suffix'] ?? '';
$quiet ?: print "quiet=$quiet\nsuffix=$suffix\n";

// Get the contents of the phpunit.xml file.
$phpunit_filename = 'phpunit.xml';
if (!file_exists($phpunit_filename)) {
  throw new RuntimeException("Unable to read $phpunit_filename");
}
$xml = new DOMDocument();
$xml->load($phpunit_filename);
$quiet ?: print_r($xml->getElementsByTagName('coverage'));
$quiet ?: print_r($xml->getElementsByTagName('source'));

$root = $xml->documentElement;
// 'coverage' is in the Drupal 10 phpunit.xml
if ($coverage = $root->getElementsByTagName('coverage')) {
  // Remove all 'coverage' as there may be more than one.
  while ($coverage->length) {
    $root->removeChild($coverage->item(0));
  }
}
// 'source' is the coverage definition in Drupal 11 phpunit.xml
if ($source = $root->getElementsByTagName('source')) {
  while ($source->length) {
    $root->removeChild($source->item(0));
  }
}
$quiet ?: print_r($xml->getElementsByTagName('coverage'));
$quiet ?: print_r($xml->getElementsByTagName('source'));

$phpunit_filename .= $suffix;
$quiet ?: print "writing to {$phpunit_filename}\n";
$xml->save($phpunit_filename);
