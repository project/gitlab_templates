<?php

/**
 * @file
 * Find unformatted http links in .md files.
 *
 * Arguments:
 *   -p --path path    Path to search in. Default is 'docs'. Do not include *.
 *                     Partial paths will be matched.
 *   -d --debug        Show debug output.
 *   -v --verbose      Show more verbose detailed output.
 */

// Get the options.
$options = getopt('p:dv', ['path:', 'debug', 'verbose']);
$verbose = array_key_exists('v', $options) ||  array_key_exists('verbose', $options);
$debug = $verbose || array_key_exists('d', $options) || array_key_exists('debug', $options);
$path = $options['p'] ?? $options['path'] ?? './docs';
!$debug ?: print "path=$path\ndebug=$debug\nverbose=$verbose\n";

$found = 0;
$code_block = FALSE;
// Allow for optional / before second { } pattern for partial paths.
$files = array_values(array_unique(glob($path . '{,/}{*.md,**/*.md}', GLOB_BRACE)));
!$debug ?: print ".md files found: " . count($files) . "\n" . print_r($files, TRUE) . "\n";

foreach ($files as $f => $filename) {
  $lines = file($filename);
  !$verbose ?: print "===== file {$f} = {$filename}\nlines=" . print_r($lines, TRUE) . PHP_EOL;

  foreach ($lines as $lnum => $text) {
    switch (TRUE) {
      // Detect when a code block begins and ends.
      case strstr($text, "```"):
        $code_block = !$code_block;
        !$verbose ?: print $lnum . ' $code_block changed to ' . ($code_block ? 'true' : 'false') . ' in: ' . $text;
        break;

      // If the line does not contain http or we are still in a code block then
      // move on to the next line.
      case !stristr($text, 'http') || $code_block:
        !$verbose ?: print $lnum . ' No http or still in code block in: ' . $text;
        break;

      // If the http is part of an inline code block then it is OK.
      case preg_match('/`.*http.*`/', $text, $matches):
        !$verbose ?: print $lnum . ' Found inline code with http so ignore: ' . $text;
        !$verbose ?: print_r($matches, TRUE);
        break;

      // If the http link is properly formatted with [text](url) then it is OK.
      // Note that this only checking the markup, not validating the url format.
      case preg_match('/\[.+\]\(http.+\)/', $text, $matches):
        !$verbose ?: print $lnum . ' Link is correctly formatted in ' . $text . '$matches=' . print_r($matches, TRUE) . PHP_EOL;
        break;

      // Report all remaining 'http' as these are formatted incorrectly.
      default:
        print str_repeat('-', 80) . "\n$filename:" . ($lnum + 1) . "\n$text";
        $found++;
        !$verbose ?: print $lnum . ' Found bad link in ' . $text . '$found=' . $found . PHP_EOL;
        break;

    }
  }
}
$found > 0 ? print str_repeat('-', 80) . "\nTo fix these links use the syntax: [text to show](url)\n" : NULL;
print "Unformatted links: Files searched in {$path}: " . count($files) . ", Issues found: {$found}\n";
$exit_code = $found ? 1 : 0;
!$debug ?: print "Ending with exit_code {$exit_code}" . PHP_EOL;
exit($exit_code);
