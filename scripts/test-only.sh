#!/bin/bash

# Script to revert all changed files that are not tests.
# This is executed using the 'source' command so that all env vars will be available.

TARGET_BRANCH=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}${CI_COMMIT_BRANCH}
BASELINE=${CI_MERGE_REQUEST_TARGET_BRANCH_SHA:-$CI_MERGE_REQUEST_DIFF_BASE_SHA}
git config --global --add safe.directory $CI_PROJECT_DIR

echo "ℹ️ Changes from ${TARGET_BRANCH} branch (SHA $BASELINE)"
git diff ${BASELINE} --stat=120,110
echo "If this list contains more files than you changed, you may need to rebase your branch."
git status

echo "1️⃣ Reverting changes that are not test files"
# In addition to the test files, do not revert changes to composer.json, .info.yml and phpunit.xml
PATTERN='(tests/|composer\.json|\.info\.yml|phpunit\.xml|\.gitlab-ci\.yml)'
CHANGES_TO_REVERT=$(git diff ${BASELINE} --diff-filter=DM --name-only | grep -Ev $PATTERN) || true
if [ "$CHANGES_TO_REVERT" != "" ]; then
  for file in $CHANGES_TO_REVERT; do
    echo "↩️ Reverting $file";
    git checkout ${BASELINE} -- $file;
  done
fi

ADDED_FILES=$(git diff ${BASELINE} --diff-filter=A --name-only | grep -Ev $PATTERN) || true
if [ "$ADDED_FILES" != "" ]; then
  for file in $ADDED_FILES; do
    echo "🗑️️ Deleting $file";
    git rm $file;
  done
fi

printf "$DIVIDER\nAfter reverting all non-test changes, the remaining changed files are:\n"
git diff ${BASELINE} --stat=120,110

PHPUNIT_OPTIONS=''
# If the project does not have its own phpunit.xml(.dist) then use the one from core. Only do this if no -c option is specified in _PHPUNIT_EXTRA.
if [[ ! $_PHPUNIT_EXTRA =~ "-c " ]]; then
  test -f "$CI_PROJECT_DIR/phpunit.xml" || test -f "$CI_PROJECT_DIR/phpunit.xml.dist" || PHPUNIT_OPTIONS="$PHPUNIT_OPTIONS -c $_WEB_ROOT/core"
fi

TESTS_CHANGED=$(git diff ${BASELINE} --name-only | grep -E 'Test\.php$') || true
if [ "$TESTS_CHANGED" != "" ]; then
  printf " \n2️⃣ Running tests changed in this merge request\n \nThe following test files have been changed, and only these tests will be run:\n$TESTS_CHANGED\n "
  for file in $TESTS_CHANGED; do
    test=$DRUPAL_PROJECT_FOLDER/$file
    if [ "$_PHPUNIT_CONCURRENT" == "0" ]; then
      printf "$DIVIDER\n_PHPUNIT_CONCURRENT=$_PHPUNIT_CONCURRENT, _PHPUNIT_EXTRA=$_PHPUNIT_EXTRA, PHPUNIT_OPTIONS=$PHPUNIT_OPTIONS\n "
      printf "executing: sudo -u www-data -H -E vendor/bin/phpunit $PHPUNIT_OPTIONS --bootstrap $PWD/$_WEB_ROOT/core/tests/bootstrap.php $test --log-junit $CI_PROJECT_DIR/junit.xml $_PHPUNIT_EXTRA\n "
      sudo -u www-data -H -E vendor/bin/phpunit $PHPUNIT_OPTIONS --bootstrap $PWD/$_WEB_ROOT/core/tests/bootstrap.php $test --log-junit $CI_PROJECT_DIR/junit.xml $_PHPUNIT_EXTRA || EXIT_CODE=$?
    elif [ "$_PHPUNIT_CONCURRENT" == "1" ]; then
      printf "$DIVIDER\n_PHPUNIT_CONCURRENT=$_PHPUNIT_CONCURRENT, _PHPUNIT_EXTRA=$_PHPUNIT_EXTRA\n "
      printf "executing: sudo -u www-data -H -E php $_WEB_ROOT/core/scripts/run-tests.sh --php '$(which php)' --color --keep-results --concurrency '32' --repeat '1' --sqlite 'sites/default/files/.sqlite' --dburl $SIMPLETEST_DB --url $SIMPLETEST_BASE_URL --xml $CI_PROJECT_DIR/$_WEB_ROOT/sites/default/files/simpletest --verbose --non-html --file $test $_PHPUNIT_EXTRA\n "
      sudo -u www-data -H -E php $_WEB_ROOT/core/scripts/run-tests.sh --php "$(which php)" --color --keep-results --concurrency "32" --repeat "1" --sqlite "sites/default/files/.sqlite" --dburl $SIMPLETEST_DB --url $SIMPLETEST_BASE_URL --xml $CI_PROJECT_DIR/$_WEB_ROOT/sites/default/files/simpletest --verbose --non-html --file $test $_PHPUNIT_EXTRA || EXIT_CODE=$?
    fi
  done;
else
  printf "$DIVIDER\n2️⃣ No test files were changed. The 'test-only changes' job can do nothing.$DIVIDER\n"
  EXIT_CODE=0
fi

cp /var/log/apache2/test.apache.access.log $CI_PROJECT_DIR/apache.access.log.txt
cp /var/log/apache2/test.apache.error.log $CI_PROJECT_DIR/apache.error.log.txt

echo "Exiting with EXIT_CODE=$EXIT_CODE"
exit $EXIT_CODE
