#!/usr/bin/env php
<?php

/**
 * @file
 * Determine which version of the templates is used.
 *
 * Optional positional arguments:
 *   1. The tag to search for. Default is the value of _GITLAB_TEMPLATES_REF.
 *   2. The project id. Default is the value of GITLAB_TEMPLATES_PROJECT_ID.
 */

/**
 * Returns an array of tags with their commit hash for the given project ID.
 *
 * @param int $gitlab_templates_project_id
 *   ID of the GitLab project within the instance.
 *
 * @return array
 *   List of tags with their commit hash.
 */
function get_tags($gitlab_templates_project_id) {
  $url = 'https://git.drupalcode.org/api/v4/projects/' . (int) $gitlab_templates_project_id . '/repository/tags';
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Curl.ProjectBrowser');
  curl_setopt($ch, CURLOPT_URL, $url);
  $response = curl_exec($ch);
  curl_close($ch);

  if (!empty($response)) {
    $tags = [];
    $json_response = json_decode($response, TRUE);
    foreach ($json_response as $tag) {
      $tags[$tag['name']] = $tag['target'];
    }

    return $tags;
  }

  return FALSE;
}

// Allow this script to run on PHP5.6 for Drupal 7.
// phpcs:disable SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator
$current_ref = isset($argv[1]) ? $argv[1] : getenv('_GITLAB_TEMPLATES_REF');
$gitlab_templates_project_id = isset($argv[2]) ? $argv[2] : getenv('GITLAB_TEMPLATES_PROJECT_ID');
if (empty($current_ref)) {
  throw new RuntimeException('Unable to determine current reference.');
}

$tags = get_tags($gitlab_templates_project_id);
if (empty($tags)) {
  echo "Could not get tags for git.drupalcode.org project with ID: $gitlab_templates_project_id.\n";
  exit;
}

$found_tag = NULL;
if (isset($tags[$current_ref])) {
  $commit_hash = $tags[$current_ref];
  // Get the keys with the matching commit hash.
  $keys = array_keys($tags, $commit_hash);
  if (!empty($keys)) {
    $found_tag = implode(', ', $keys);
  }
}

$version = $found_tag ? $found_tag : "$current_ref (branch)";

echo "GitLab Templates version: $version\n";
exit;
