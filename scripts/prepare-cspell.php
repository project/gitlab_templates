#!/usr/bin/env php
<?php

/**
 * @file
 * Prepares a .cspell.json file customized for the gitlab templates environment.
 *
 * Arguments:
 *
 *   -s --suffix    Optional suffix to append to the input filename before
 *                  writing out. Useful when running locally during development
 *                  to avoid overwriting the input .cspell.json file.
 *
 *   -v --verbose   Show verbose debug output.
 */

// Get the arguments.
$options = getopt('s:v', ['suffix', 'verbose']);
$quiet = !array_key_exists('v', $options) && !array_key_exists('verbose', $options);
$suffix = $options['s'] ?? $options['suffix'] ?? '';
$quiet ?: print '$suffix=' . $suffix . PHP_EOL;

// Get the contents of .cspell.json into an array. This file will be either the
// projects own .cspell.json or the default copied from /assets.
$cspell_filename = '.cspell.json';
$cspell_json = json_decode(file_get_contents($cspell_filename), TRUE);
if (empty($cspell_json)) {
  throw new RuntimeException("Unable to read $cspell_filename");
}
$quiet ?: print 'At start cspell_json=' . print_r($cspell_json, TRUE) . PHP_EOL;

// Some directories in the project root are not part of the project.
$webRoot = getenv('_WEB_ROOT') ?: 'web';
$non_project_directories = ["$webRoot", 'vendor', 'node_modules', '.git'];

// Specify the files that are always ignored.
$ignore_files = [
  // *.patch will match files in the top level and all sub-directories.
  '*.patch',
  '*.svg',
  '*_ignore_*',
];

// Specify the standard files to ignore if _CSPELL_IGNORE_STANDARD_FILES=1.
$ignore_standard_files = [
  '**/.*.json',
  'package.json',
  'yarn.lock',
  'phpstan*',
  '.*ignore',
];

// This list of files is also ignored when _CSPELL_IGNORE_STANDARD_FILES=1 but
// we need to find the actual case-sensitive names for these.
$filenames_to_find = [
  'license',
  'copyright',
  'maintainers',
  'changelog',
  'composer',
];

// -----
// Words
// -----
//
// Get the words from $_CSPELL_WORDS.
if ($cspell_words = getenv('_CSPELL_WORDS')) {
  $quiet ?: print 'Initial $cspell_words=' . $cspell_words . PHP_EOL;
  // Remove all double quotes and spaces.
  $cspell_words = str_replace(['"', ' '], ['', ''], $cspell_words);
  // Remove single quotes from start and end of words, but not from the middle.
  $words = str_replace([",'", "',"], [',', ','], ',' . $cspell_words . ',');
  $quiet ?: print '$words=' . $words . PHP_EOL;
}

// The module's machine name might not be a real word, so add this. The value of
// $CI_PROJECT_NAME can end -nnnnnnn so remove this part.
$module_name_parts = explode('_', stristr(getenv('CI_PROJECT_NAME') . '-', '-', TRUE));
// Also find of all the sub-modules by looking for .info.yml files, and add each
// name part to the allowed list.
foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator('.', RecursiveDirectoryIterator::SKIP_DOTS)) as $file) {
  // Ignore everything in these folders.
  foreach ($non_project_directories as $dir) {
    if (stripos($file, "./$dir/") !== FALSE) {
      continue 2;
    }
  }
  if (stripos($file->getFilename(), '.info.yml') !== FALSE) {
    // Get each underscore-separated part of the module name.
    $module_name_parts = array_merge(
      $module_name_parts,
      explode('_', str_replace('.info.yml', '', $file->getFilename())),
    );
  }
  // Identify files with a name in this list, regardless of case or extension.
  // This is not part of the 'Words' section, but it is convenient to use this
  // foreach loop, instead of traversing the directory a second time in the
  // 'Ignore paths' section.
  $filename_without_ext = strtolower(substr($file->getFilename(), 0, strpos($file->getFilename(), '.')));
  if (in_array($filename_without_ext, $filenames_to_find)) {
    $ignore_standard_files[] = $file->getPathname();
  }
}
$quiet ?: print '$module_name_parts=' . print_r($module_name_parts, TRUE) . PHP_EOL;

// Merge into the existing json 'words' value, but cater for that being empty.
// array_values() is needed after array_unique() to restore the keys to numeric.
$cspell_json['words'] = array_values(array_filter(array_unique(array_merge(
  $cspell_json['words'] ?? [],
  array_filter(explode(',', $words ?? '')),
  $module_name_parts,
  // Add lando and ddev to cater for references to local docker integrations.
  ['lando', 'ddev'],
  // Add some common words that were dropped from core dictionaries.
  // See https://www.drupal.org/project/gitlab_templates/issues/3494834
  ['endapply', 'nightwatchjs', 'testgroup', 'testgroups', 'backlinks', 'mdhash', 'uploaders', 'vocabs'],
  // Add Curl options and constants dropped from core dictionaries.
  ['curlopt', 'returntransfer', 'curle', 'customrequest', 'failonerror', 'postfields', 'followlocation'],
  // Add six phpcs sniff names/error codes that cannot be corrected.
  // This can be removed if/when the core issue is resolved.
  // See https://www.drupal.org/project/drupal/issues/3418190
  [
    'colourdefinitionsniff', 'seletorsingleline', 'teamplatespacingaftercomment', 'tforvalue', 'trhowscommentindentation',
    'unecessaryfiledeclaration',
  ],
))));
$quiet ?: print '$cspell_json[\'words\']=' . print_r($cspell_json['words'], TRUE) . PHP_EOL;

// ----------
// Flag Words
// ----------
//
// Get any flagged words from $_CSPELL_FLAGWORDS.
if ($cspell_flagwords = getenv('_CSPELL_FLAGWORDS')) {
  $quiet ?: print 'Input $cspell_flagwords=' . $cspell_flagwords . PHP_EOL;
  // Remove any quotes and spaces. Double quotes are added in json_encode.
  $cspell_flagwords = str_replace(["'", '"', ' '], ['', '', ''], $cspell_flagwords);
  $cspell_json['flagWords'] = array_values(array_unique(array_merge(
    $cspell_json['flagWords'] ?? [],
    array_filter(explode(',', $cspell_flagwords)),
  )));
  $quiet ?: print '$cspell_json[\'flagWords\']=' . print_r($cspell_json['flagWords'], TRUE) . PHP_EOL;
}

// ------------
// Ignore Paths
// ------------
//
// Ignore the paths in the project root folder that are not part of the project.
$paths = $non_project_directories;

// Add the paths that are always ignored.
$paths = array_merge($paths, $ignore_files);

// Add these commonly ignored paths and files if required.
$cspell_ignore_standard_files = getenv('_CSPELL_IGNORE_STANDARD_FILES');
if ("$cspell_ignore_standard_files" != "0") {
  $paths = array_merge($paths, $ignore_standard_files);
}
// Add the output file, so that it is not counted and checked on second run.
$paths[] = '_cspell_unrecognized_words.txt';

// Add the values from $_CSPELL_IGNORE_PATHS, removing quotes and spaces.
if ($cspell_ignore_paths = getenv('_CSPELL_IGNORE_PATHS')) {
  $cspell_ignore_paths = str_replace(["'", '"', ' '], ['', '', ''], $cspell_ignore_paths);
}
$cspell_json['ignorePaths'] = array_values(array_unique(array_merge(
  $cspell_json['ignorePaths'] ?? [],
  $paths,
  $cspell_ignore_paths ? explode(',', $cspell_ignore_paths) : [],
)));

// ------------
// Dictionaries
// ------------
$dictionary_definitions = [
  [
    'name' => 'drupal',
    'path' => $webRoot . '/core/misc/cspell/drupal-dictionary.txt',
  ],
  [
    'name' => 'dictionary',
    'path' => $webRoot . '/core/misc/cspell/dictionary.txt',
  ],
];
if ($project_dictionary = getenv('_CSPELL_DICTIONARY')) {
  $quiet ?: print '$project_dictionary=' . $project_dictionary . PHP_EOL;
  $dictionary_definitions[] = [
    'name' => 'project-words',
    'path' => './' . $project_dictionary,
    'description' => "The project's own custom dictionary (optional)",
  ];
}
$quiet ?: print 'Initial $dictionary_definitions=' . print_r($dictionary_definitions, TRUE) . PHP_EOL;

$dictionary_names = [];
foreach ($dictionary_definitions as $key => $data) {
  // Add the 'name' if the file exists. Remove the array entry if it does not.
  if (file_exists($data['path'])) {
    $dictionary_names[] = $data['name'];
  }
  else {
    unset($dictionary_definitions[$key]);
  }
}
$quiet ?: print 'After checking files, $dictionary_definitions=' . print_r($dictionary_definitions, TRUE) . PHP_EOL;

// These dictionaries are provided by CSpell.
$built_in_dictionaries = [
  'companies',
  'fonts',
  'html',
  'php',
  'softwareTerms',
  'misc',
  'typescript',
  'node',
  'css',
  'bash',
  'filetypes',
  'npm',
  'lorem-ipsum',
  'sql',
  'fullstack',
];
$cspell_json['dictionaries'] = array_values(array_unique(array_merge(
  $cspell_json['dictionaries'] ?? [],
  $built_in_dictionaries,
  $dictionary_names,
)));

// Remove any matching entries so that the updated versions take precedence.
foreach ($cspell_json['dictionaryDefinitions'] ?? [] as $key => $dic) {
  if (in_array($dic['name'], $dictionary_names)) {
    unset($cspell_json['dictionaryDefinitions'][$key]);
  }
}
$cspell_json['dictionaryDefinitions'] = merge_deep($dictionary_definitions, $cspell_json['dictionaryDefinitions'] ?? []);
$quiet ?: print '$cspell_json[\'dictionaryDefinitions\']=' . print_r($cspell_json['dictionaryDefinitions'], TRUE) . PHP_EOL;

// ---------------------------
// Write out the modified file
// ---------------------------
// Allow for easy testing by avoiding overwriting the input file.
$cspell_filename .= $suffix;
$quiet ?: print 'At end $cspell_json=' . print_r($cspell_json, TRUE) . PHP_EOL;
print "Writing json array to {$cspell_filename}" . PHP_EOL;
file_put_contents($cspell_filename, json_encode($cspell_json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

/**
 * Deeply merges arrays. Borrowed from Drupal core.
 */
function merge_deep(): array {
  return merge_deep_array(func_get_args());
}

/**
 * Deeply merges arrays. Borrowed from drupal.org/project/core.
 *
 * @param array $arrays
 *   An array of array that will be merged.
 * @param bool $preserve_integer_keys
 *   Whether to preserve integer keys.
 */
function merge_deep_array(array $arrays, bool $preserve_integer_keys = FALSE): array {
  $result = [];
  foreach ($arrays as $array) {
    foreach ($array as $key => $value) {
      if (is_int($key) && !$preserve_integer_keys) {
        $result[] = $value;
      }
      elseif (isset($result[$key]) && is_array($result[$key]) && is_array($value)) {
        $result[$key] = merge_deep_array([$result[$key], $value], $preserve_integer_keys);
      }
      else {
        $result[$key] = $value;
      }
    }
  }
  return $result;
}
