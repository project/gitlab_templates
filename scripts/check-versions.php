<?php

/**
 * @file
 * Check that the version variables are up-to-date.
 *
 * This script will fail with a non-zero exit code if any of the CORE_ variable
 * values do not match the actual latest releases or expected derived values.
 * See https://www.drupal.org/project/drupal for the latest releases.
 */

// phpcs:disable DrupalPractice.Constants.GlobalConstant.GlobalConstant

const RELEASES_ENDPOINT = 'https://updates.drupal.org/release-history/drupal/current';
// https://www.drupal.org/about/core/blog/new-drupal-core-branching-scheme-introduced
const MAIN_BRANCH = '11.x';

// These are non-existent tags, they are the "future" stable and dev releases
// that will trigger this script to fail. When any of the below releases are
// found, it means that we need to do changes to the hidden-variables file
// and/or these constants.
const NEXT_D7 = '7.104';
const NEXT_MAJOR_DEV = '12.x';
const NEXT_MAJOR_FIRST_STABLE_TAG = '12.0.0';
const NEXT_MINOR_DEV = '11.2.x';
const NEXT_MINOR_STABLE = '11.2.0';

/**
 * Gather releases and store them in arrays.
 */
function gather_releases(&$dev_releases, &$security_releases, &$stable_tags) {
  echo "Gathering releases from " . RELEASES_ENDPOINT . ":" . PHP_EOL;

  // Parse for D8+ info on releases.
  $xml = simplexml_load_file(RELEASES_ENDPOINT);
  if (!$xml) {
    echo "- [ERROR] Could not parse new Drupal versions. Check the endpoint is reachable." . PHP_EOL;
    return 1;
  }

  $array = json_decode(json_encode($xml), TRUE);
  if (empty($array['releases']) || empty($array['supported_branches'])) {
    echo "- [ERROR] Could not parse new Drupal versions. Check the endpoint is returning correct data." . PHP_EOL;
    return 1;
  }

  $supported = str_replace(',', '|', $array['supported_branches']);
  $releases = $array['releases']['release'];
  $return = 0;

  foreach ($releases as $release) {
    $tag = $release['tag'];
    $terms = $release['terms']['term'] ?? [];
    // When there is only one term, make it an array.
    if (!empty($terms['name'])) {
      $terms = [$terms];
    }

    $non_existent = [NEXT_MAJOR_FIRST_STABLE_TAG, NEXT_MAJOR_DEV, NEXT_MINOR_STABLE, NEXT_MINOR_DEV];
    if (($found_next = array_search($tag, $non_existent)) !== FALSE) {
      $constants = ['NEXT_MAJOR_FIRST_STABLE_TAG', 'NEXT_MAJOR_DEV', 'NEXT_MINOR_STABLE', 'NEXT_MINOR_DEV'];
      $update_next = $constants[$found_next];
      echo "- [ERROR] Drupal $tag is out!" . PHP_EOL;
      echo "- [ERROR] The value of {$update_next} is currently " . constant($update_next) . " - this needs to be updated." . PHP_EOL . ' ' . PHP_EOL;
      $return = 1;
    }

    if (str_contains($tag, '.x')) {
      $dev_releases[] = $tag;
    }
    elseif (!str_contains($tag, '-')) {
      // Only worry about supported tags.
      if (preg_match('/^' . $supported . '*/', $tag)) {
        $stable_tags[] = $tag;
        foreach ($terms as $term) {
          if ($term['name'] == 'Release type' && $term['value'] == 'Security update') {
            $security_releases[] = $tag;
          }
        }
      }
    }
  }

  // Order arrays by most recent release.
  usort($stable_tags, 'reverse_version_compare');
  usort($security_releases, 'reverse_version_compare');
  usort($dev_releases, 'reverse_version_compare');

  echo "- Stable tags: " . implode(', ', $stable_tags) . PHP_EOL;
  echo "- Security release tags: " . implode(', ', $security_releases) . PHP_EOL;
  echo "- Dev releases: " . implode(', ', $dev_releases) . PHP_EOL . ' ' . PHP_EOL;

  return $return;
}

/**
 * Check whether the URL returns a 200 status code.
 */
function check_url($url) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_NOBODY, TRUE);
  curl_exec($ch);
  $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  return ($return_code == 200);
}

/**
 * Reverse of the native version_compare function.
 */
function reverse_version_compare($a, $b) {
  return version_compare($b, $a);
}

/**
 * Get the latest version of an ordered list in string or array format.
 */
function get_latest($versions, $as_semver_array = FALSE) {
  // Use array_shift to allow for a non-zero keyed first item.
  $latest = array_shift($versions);
  $semver_latest_array = explode('.', $latest);

  return $as_semver_array ? $semver_latest_array : $latest;
}

/**
 * Check CORE_STABLE variable.
 */
function check_core_stable($stable_tags) {
  echo "Logic for CORE_STABLE." . PHP_EOL;
  $latest_stable = get_latest($stable_tags);
  if (getenv('CORE_STABLE') != $latest_stable) {
    echo "- [ERROR] CORE_STABLE (" . getenv('CORE_STABLE') . ") does not match the latest stable release: $latest_stable." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_SECURITY variable.
 */
function check_core_security($security_releases, $stable_tags) {
  echo "Logic for CORE_SECURITY." . PHP_EOL;
  $latest_security = get_latest($security_releases);
  if (getenv('CORE_SECURITY') != $latest_security) {
    // The only exception allowed is when CORE_SECURITY is in the same minor as
    // CORE_STABLE and the latest security tag is in a smaller minor.
    $semver_core_security_array = explode('.', getenv('CORE_SECURITY'));
    $semver_core_stable_array = explode('.', getenv('CORE_STABLE'));
    if (
      // Different majors.
      $semver_core_security_array[0] != $semver_core_stable_array[0] ||
      // Same majors, but security on different minor.
      $semver_core_security_array[1] != $semver_core_stable_array[1]
    ) {
      echo "- [ERROR] CORE_STABLE (" . getenv('CORE_STABLE') . ") and CORE_SECURITY (" . getenv('CORE_SECURITY') . ") have different major:minor versions." . PHP_EOL . ' ' . PHP_EOL;
      return 1;
    }

    if (getenv('CORE_SECURITY') == getenv('CORE_SECURITY_PREVIOUS_MINOR')) {
      echo "- [ERROR] CORE_SECURITY (" . getenv('CORE_SECURITY') . ") should be different from CORE_SECURITY_PREVIOUS_MINOR (" . getenv('CORE_SECURITY_PREVIOUS_MINOR') . ")" . PHP_EOL . ' ' . PHP_EOL;
      return 1;
    }
  }

  return 0;
}

/**
 * Check CORE_SECURITY_PREVIOUS_MINOR variable.
 */
function check_core_security_previous_minor($security_releases, $stable_tags) {
  echo "Logic for CORE_SECURITY_PREVIOUS_MINOR." . PHP_EOL;

  $semver_latest_stable_array = get_latest($stable_tags, TRUE);
  $core_security_previous_minor = FALSE;
  if ((int) $semver_latest_stable_array[1] > 0) {
    $previous_security_major_minor = $semver_latest_stable_array[0] . '.' . ($semver_latest_stable_array[1] - 1) . '.';
  }
  else {
    // CORE_SECURITY_PREVIOUS_MINOR can be empty but only when there is no
    // previous minor on the same major branch.
    if (getenv('CORE_SECURITY_PREVIOUS_MINOR') != '') {
      echo "- [ERROR] CORE_SECURITY_PREVIOUS_MINOR (" . getenv('CORE_SECURITY_PREVIOUS_MINOR') . ") should be blank." . PHP_EOL . ' ' . PHP_EOL;
      return 1;
    }
    else {
      return 0;
    }
  }

  foreach ($security_releases as $security_release) {
    if (
      !$core_security_previous_minor &&
      str_starts_with($security_release, $previous_security_major_minor)
    ) {
      $core_security_previous_minor = $security_release;
    }
  }
  if (getenv('CORE_SECURITY_PREVIOUS_MINOR') != $core_security_previous_minor) {
    echo "- [ERROR] CORE_SECURITY_PREVIOUS_MINOR (" . getenv('CORE_SECURITY_PREVIOUS_MINOR') . ") does not match the expected value: $core_security_previous_minor." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_SUPPORTED variable.
 */
function check_core_supported($stable_tags, $dev_releases) {
  echo "Logic for CORE_SUPPORTED." . PHP_EOL;

  // Same major:minor as CORE_STABLE but ending in "x".
  $semver_latest_stable_array = get_latest($stable_tags, TRUE);
  $expected_value = $semver_latest_stable_array[0] . '.' . $semver_latest_stable_array[1] . '.x';
  if (getenv('CORE_SUPPORTED') != $expected_value . '-dev') {
    echo "- [ERROR] CORE_SUPPORTED (" . getenv('CORE_SUPPORTED') . ") does not match the expected value: $expected_value-dev." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  if (!in_array($expected_value, $dev_releases)) {
    echo "- [ERROR] $expected_value is not a valid dev release." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_NEXT_MINOR variable.
 */
function check_core_next_minor($stable_tags, $dev_releases) {
  echo "Logic for CORE_NEXT_MINOR." . PHP_EOL;

  // CORE_NEXT_MINOR can be empty or can match the MAIN_BRANCH.
  if (empty(getenv('CORE_NEXT_MINOR')) || getenv('CORE_NEXT_MINOR') == MAIN_BRANCH . '-dev') {
    return 0;
  }

  // Same major as CORE_STABLE but one number up on minor.
  $semver_latest_stable_array = get_latest($stable_tags, TRUE);
  $expected_value = $semver_latest_stable_array[0] . '.' . ($semver_latest_stable_array[1] + 1) . '.x';

  if (getenv('CORE_NEXT_MINOR') != $expected_value . '-dev') {
    echo "- [ERROR] CORE_NEXT_MINOR (" . getenv('CORE_NEXT_MINOR') . ") does not match the expected value: $expected_value-dev." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  if (!in_array($expected_value, $dev_releases)) {
    echo "- [ERROR] $expected_value is not a valid dev release." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_MINOR variable.
 */
function check_core_minor($dev_releases) {
  echo "Logic for CORE_MINOR." . PHP_EOL;

  // Next core minors are branched from the main branch.
  $expected_value = MAIN_BRANCH;
  if (getenv('CORE_MINOR') != $expected_value . '-dev') {
    echo "- [ERROR] CORE_MINOR (" . getenv('CORE_MINOR') . ") does not match the expected value: $expected_value-dev." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  if (!in_array($expected_value, $dev_releases)) {
    echo "- [ERROR] $expected_value is not a valid dev release." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_MAJOR_DEVELOPMENT variable.
 */
function check_core_major_development($dev_releases) {
  echo "Logic for CORE_MAJOR_DEVELOPMENT." . PHP_EOL;

  // CORE_MAJOR_DEVELOPMENT can be empty.
  if (empty(getenv('CORE_MAJOR_DEVELOPMENT'))) {
    return 0;
  }

  // Next core majors are branched from the main branch.
  $expected_value = NEXT_MAJOR_DEV;
  if (getenv('CORE_MAJOR_DEVELOPMENT') != $expected_value . '-dev') {
    echo "- [ERROR] CORE_MAJOR_DEVELOPMENT (" . getenv('CORE_MAJOR_DEVELOPMENT') . ") does not match the expected value: $expected_value-dev." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  if (!in_array($expected_value, $dev_releases)) {
    echo "- [ERROR] $expected_value is not a valid dev release." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Check CORE_PREVIOUS_STABLE variable.
 */
function check_core_previous_stable($stable_tags) {
  echo "Logic for CORE_PREVIOUS_STABLE." . PHP_EOL;

  // CORE_PREVIOUS_STABLE can be empty.
  if (empty(getenv('CORE_PREVIOUS_STABLE'))) {
    return 0;
  }

  $latest_core = get_latest($stable_tags, TRUE)[0];
  // Remove all versions that do not start with the latest core major. The first
  // of those remaining will be the highest previous stable version.
  $previous_stable_array = array_filter($stable_tags, function ($x) use ($latest_core) {
    return strpos($x, "$latest_core.") === FALSE;
  });
  $expected_value = get_latest($previous_stable_array);
  if (getenv('CORE_PREVIOUS_STABLE') != $expected_value) {
    echo "- [ERROR] CORE_PREVIOUS_STABLE (" . getenv('CORE_PREVIOUS_STABLE') . ") does not match the expected value: $expected_value" . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }
}

/**
 * Check for a new Drupal 7 release.
 */
function check_core_legacy_stable() {
  echo "Logic for CORE_LEG_STABLE." . PHP_EOL;
  if (!is_numeric(getenv('CORE_LEG_STABLE'))) {
    echo "- [ERROR] CORE_LEG_STABLE (" . getenv('CORE_LEG_STABLE') . ") must be a numeric value." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  // Check that NEXT_D7 is actually greater than CORE_LEG_STABLE.
  if (version_compare(getenv('CORE_LEG_STABLE'), NEXT_D7, '>=')) {
    echo "- [ERROR] CORE_LEG_STABLE (" . getenv('CORE_LEG_STABLE') . ") and script variable NEXT_D7 (" . NEXT_D7 . ") are not consitent with each other." . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  $next_d7_url = 'https://ftp.drupal.org/files/projects/drupal-' . NEXT_D7 . '.zip';
  if (check_url($next_d7_url)) {
    echo "- [ERROR] CORE_LEG_STABLE = " . getenv('CORE_LEG_STABLE') . ". There is a new release of Drupal 7 available: " . NEXT_D7 . PHP_EOL . ' ' . PHP_EOL;
    return 1;
  }

  return 0;
}

/**
 * Load env variables from file.
 *
 * @see https://dev.to/fadymr/php-create-your-own-php-dotenv-3k2i
 */
function load_dot_env($file) {
  if (file_exists($file)) {
    $lines = file($file);
    foreach ($lines as $line) {
      $line = trim($line);
      if (empty($line) || strpos($line, '#') === 0) {
        continue;
      }

      [$name, $value] = explode('=', $line, 2);
      $name = trim($name);
      $value = trim($value);
      set_env_var($name, $value);
    }
  }
}

/**
 * Load variables yml file.
 */
function load_variables_file($file) {
  if (file_exists($file)) {
    $lines = file($file);
    $values = [];
    $last_name = '';
    foreach ($lines as $line) {
      $line = trim($line);
      if (empty($line) || strpos($line, '#') === 0 || strpos($line, 'description:') === 0) {
        continue;
      }

      // Remaining lines are either the name of the variable, the value or both.
      if (strpos($line, 'value:') === 0) {
        // Remove 'value:' and all quotes.
        $values[$last_name] = trim(str_replace(['value:', '"', "'"], ['', '', ''], $line));
      }
      else {
        $last_name = trim($line, ':');
        // If the line contains the name and a value then get both.
        $name_and_value = explode(':', $line);
        if (!empty($name_and_value[1])) {
          $last_name = $name_and_value[0];
          $values[$last_name] = trim(str_replace(['"', "'"], ['', ''], $name_and_value[1]));
        }
      }
    }

    foreach ($values as $name => $value) {
      set_env_var($name, $value);
    }
  }
}

/**
 * Sets the environment variable.
 */
function set_env_var($name, $value) {
  putenv(sprintf('%s=%s', $name, $value));
  $_ENV[$name] = $value;
  $_SERVER[$name] = $value;
}

/**
 * Load the variables defined in the variables file as env variables.
 *
 * Allow overrides via .env files too.
 */
function load_variables() {
  load_variables_file(__DIR__ . '/../includes/include.drupalci.hidden-variables.yml');
  load_dot_env(__DIR__ . '/.env');
}

/**
 * Show current values for the variables we will check.
 */
function show_current_values() {
  echo PHP_EOL . 'Current values of the variables             Variant where this is used' . PHP_EOL;
  echo '- CORE_STABLE:                  ' . str_pad(getenv('CORE_STABLE') ?: '<empty>', 12) . 'Current and max PHP' . PHP_EOL;
  echo '- CORE_SECURITY:                ' . (getenv('CORE_SECURITY') ?: '<empty>') . PHP_EOL;
  echo '- CORE_SUPPORTED:               ' . str_pad(getenv('CORE_SUPPORTED') ?: '<empty>', 12) . PHP_EOL;
  echo '- CORE_SECURITY_PREVIOUS_MINOR: ' . str_pad(getenv('CORE_SECURITY_PREVIOUS_MINOR') ?: '<empty>', 12) . 'Previous Minor' . PHP_EOL;
  echo '- CORE_PREVIOUS_STABLE:         ' . str_pad(getenv('CORE_PREVIOUS_STABLE') ?: '<empty>', 12) . 'Previous Major' . PHP_EOL;
  echo '- CORE_MINOR:                   ' . (getenv('CORE_MINOR') ?: '<empty>') . PHP_EOL;
  echo '- CORE_NEXT_MINOR:              ' . str_pad(getenv('CORE_NEXT_MINOR') ?: '<empty>', 12) . 'Next Minor' . PHP_EOL;
  echo '- CORE_MAJOR_DEVELOPMENT:       ' . str_pad(getenv('CORE_MAJOR_DEVELOPMENT') ?: '<empty>', 12) . 'Next Major' . PHP_EOL;
  echo '- CORE_LEG_STABLE:              ' . str_pad(getenv('CORE_LEG_STABLE') ?: '<empty>', 12) . 'Drupal 7 tests' . PHP_EOL . ' ' . PHP_EOL;

  echo 'Internal constants defined for future versions that do not exist yet' . PHP_EOL;
  echo '- NEXT_MINOR_DEV:               ' . NEXT_MINOR_DEV . PHP_EOL;
  echo '- NEXT_MINOR_STABLE:            ' . NEXT_MINOR_STABLE . PHP_EOL;
  echo '- NEXT_MAJOR_DEV:               ' . NEXT_MAJOR_DEV . PHP_EOL;
  echo '- NEXT_MAJOR_FIRST_STABLE_TAG:  ' . NEXT_MAJOR_FIRST_STABLE_TAG . PHP_EOL;
  echo '- NEXT_D7:                      ' . NEXT_D7 . PHP_EOL . ' ' . PHP_EOL;
}

// =============================================================================
// Executable code starts here.
// =============================================================================
echo '>>> Executing script: check-versions.php' . PHP_EOL;
$dev_releases = [];
$security_releases = [];
$stable_tags = [];

// Step 0: Load variables.
load_variables();
show_current_values();

// Step 1: Gather info.
if (($exit_code = gather_releases($dev_releases, $security_releases, $stable_tags)) == 0) {
  // Step 2: Drupal 10+ CORE_* variables.
  $exit_code =
    check_core_stable($stable_tags) +
    check_core_security($security_releases, $stable_tags) +
    check_core_supported($stable_tags, $dev_releases) +
    check_core_security_previous_minor($security_releases, $stable_tags) +
    check_core_previous_stable($stable_tags) +
    check_core_minor($dev_releases) +
    check_core_next_minor($stable_tags, $dev_releases) +
    check_core_major_development($dev_releases) +
    // Step 3: Drupal 7 CORE_* variable.
    check_core_legacy_stable();
}

echo PHP_EOL . ($exit_code == 0 ? 'All values are correct' : "CHECK THE [ERROR] MESSAGES ABOVE\nTo test changes locally run `php scripts/check-versions.php`") . PHP_EOL;
echo "Ending with exit_code {$exit_code}" . PHP_EOL;
exit($exit_code);
