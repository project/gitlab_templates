#!/usr/bin/env php
<?php

/**
 * @file
 * Symlink the files from this project into Drupal's modules/custom directory.
 *
 * Param 1 = Project name
 * Param 2 = Path from webRoot to modules directory. This will vary depending on
 *           whether running a Drupal 7 or Drupal 10 pipeline. If none supplied
 *           then default to the Drupal 10 path.
 *
 * This script is also used for Drupal 7, which can go as low as PHP5.6, so do
 * not use new PHP features and make sure that any change here is tested
 * against PHP5.6. Therefore specific phpcs rules are disabled for this file.
 */

// phpcs:disable SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator
// phpcs:disable Drupal.Arrays.Array.CommaLastItem

use Symfony\Component\Filesystem\Filesystem;

require __DIR__ . '/vendor/autoload.php';
$project_name = isset($argv[1]) ? $argv[1] : getenv('CI_PROJECT_NAME');
$pathToModules = isset($argv[2]) ? $argv[2] : 'modules/custom';

if (empty($project_name)) {
  throw new RuntimeException('Unable to determine project name.');
}
$fs = new Filesystem();

// Directory where the root project is being created.
$projectRoot = getcwd();

// Directory where the contrib module symlinks are to be created.
$webRoot = getenv('_WEB_ROOT') ?: 'web';
$moduleRoot = $projectRoot . '/' . $webRoot . '/' . $pathToModules . '/' . $project_name;

// Prepare directory for current module.
if ($fs->exists($moduleRoot)) {
  $fs->remove($moduleRoot);
}
$fs->mkdir($moduleRoot);
$rel = $fs->makePathRelative($projectRoot, $moduleRoot);

// Retrieve the saved list of original files and folders that need symlinks, to
// avoid linking to new files and folders that do not belong to the project.
// Do not need links to the .git and .idea folders and build.env file.
$projectFiles = array_diff(explode("\n", getenv('PROJECT_FILES')), ['.git', '.idea', 'build.env']);

// For backwards-compatibility if the variable is undefined or blank then get
// the current list and remove known unwanted folders and files.
if (empty(array_diff($projectFiles, ['']))) {
  $projectFiles = array_diff(scandir($projectRoot), [
    '.',
    '..',
    '.git',
    '.idea',
    'build.env',
    'drush',
    'vendor',
    $webRoot,
    'symlink_project.php',
    'expand_composer_json.php',
    'composer.json.backup'
  ]);
  $divider = str_repeat('*', 120) . "\n";
  print "{$divider}WARNING: The list of project files and folders has been derived here, because the environment variable PROJECT_FILES "
    . "is empty.\n" . print_r($projectFiles, TRUE) . "\nThis may not be accurate and to avoid this warning, at the start of your "
    . "customized composer script execute the following command:\n\nexport PROJECT_FILES=$(ls -A)\n\n"
    . "See change record https://www.drupal.org/node/3500320 for more details\n{$divider}\n";
}

echo "Creating symlinks in $moduleRoot pointing back to files in $projectRoot\n";
foreach (scandir($projectRoot) as $item) {
  if (in_array($item, $projectFiles)) {
    $fs->symlink($rel . $item, $moduleRoot . "/$item");
    print $item . PHP_EOL;
  }
}
