#!/bin/bash

# Launch from the root of this project: "./scripts/run-local-checks.sh [fix] [debug] [clean]".
#
# Optional arguments:
#   fix    - to run the fixer options of eslint and phpcs.
#   debug  - to show verbose output.
#   clean  - to remove the installed software and files copied from assets.
#
# Helper script to run all the checks that will be run via GitLab CI.
#
# It assumes that you have:
# - node (version 18+)
# - npm (version 9+)
# - composer 2
# - shellcheck

# Copy configuration files for the tools.
cp -a ./assets/internal/. .

# Run prepare-cspell to allow testing with the modified .cspell.json file.
export _CSPELL_IGNORE_STANDARD_FILES=0
php scripts/prepare-cspell.php

# Run the tools.
npm install
composer install
EXIT_CODE=0
NO_PROGRESS=$([[ "$1" == "debug" ]] && echo "" || echo "--no-progress");
npx cspell --show-suggestions --show-context --dot $NO_PROGRESS {**,.**} || EXIT_CODE=$((EXIT_CODE+1))
ESLINT_FIX=$([[ "$1" == "fix" ]] && echo "--fix" || echo "");
DEBUG=$([[ "$1" == "debug" ]] && echo "--debug" || echo "");
npx eslint --no-error-on-unmatched-pattern --ignore-pattern=vendor --ignore-pattern=node_modules --ext=.yml $DEBUG $ESLINT_FIX . && echo "ESLint passed." || EXIT_CODE=$((EXIT_CODE+1))
[[ "$1" == "fix" ]] && vendor/bin/phpcbf --colors scripts/*.php -s
VERBOSE=$([[ "$1" == "debug" ]] && echo "-v" || echo "");
vendor/bin/phpcs --colors scripts/*.php -s $VERBOSE && echo "PHPCS passed." || EXIT_CODE=$((EXIT_CODE+1))
shellcheck scripts/*.sh && echo "Shellcheck passed." || EXIT_CODE=$((EXIT_CODE+1))
php scripts/unformatted-links.php $VERBOSE || EXIT_CODE=$((EXIT_CODE+1))
[[ "$EXIT_CODE" != 0 ]] && echo -e "\n===========================\nNumber of failed checks: $EXIT_CODE\n===========================\n" || echo "= All OK ="

# Clean up all files that were copied.
if [[ "$1" == "clean" ]]; then
  rm -rf node_modules/ vendor/
  rm .cspell.json .eslintrc.js .shellcheckrc .project-words.txt composer.json composer.lock package.json package-lock.json phpcs.xml .prettierrc.json
fi
