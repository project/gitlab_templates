#!/bin/bash

# Usage: ./scripts/set-default-tag.sh 1.1.0 [push]

BRANCH="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$BRANCH" != "main" ]]; then
  echo 'Tags should be set on the "main" branch.'
  exit 1
fi

if [[ -z "$1" ]]; then
  echo "No argument supplied"
  exit 1
fi

if [[ "$2" == "push" ]]; then
  DRY_RUN=0
  echo "Dry-run mode is OFF. Git commands will be executed."
else
  DRY_RUN=1
  echo "Dry-run mode is ON. No git commands will be executed."
fi

TAG="$1"
DEFAULT_TAG="default-ref"

echo "Setting default tag to be the same as: $TAG"
if [[ "$DRY_RUN" == "1" ]]; then
  echo "# Checkout the tag."
  echo "- git checkout $TAG"

  echo "# Override the default one."
  echo "- git tag -d $DEFAULT_TAG || TRUE"
  echo "- git push origin --delete $DEFAULT_TAG || TRUE"
  echo "- git tag $DEFAULT_TAG"
  echo "- git push origin $DEFAULT_TAG"

  echo "# Back to the main branch."
  echo "- git checkout main"
else
  # Checkout the tag.
  git checkout $TAG

  # Override the default one.
  git tag -d $DEFAULT_TAG || TRUE
  git push origin --delete $DEFAULT_TAG || TRUE
  git tag $DEFAULT_TAG
  git push origin $DEFAULT_TAG

  # Back to the main branch.
  git checkout main
fi
