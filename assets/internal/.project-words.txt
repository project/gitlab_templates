# Words in the docs folder.
gitlab
drupal
phpunit
stylelint
camelcase
cspell
nightwatch
javascript
dropzonejs
redis
dockerfile
predis
berdir
phpize
phpcs
phpcbf
drupalci
devel
phpstan
mkdocs
mdspell
eslint
drupal.org
json
mglaman
firecow
chromedriver
locahost
shellcheckrc
macos
homebrew
codebase
semver
cron
localhost
urls
readme
uncomment
testdox
doctum
phpdox
documentor
# Invented words for the example of _cspell_words.
mycustomthing
madeupword

# Words in the rest of the code.
autobuild
basepath
browsertest
codeclimate
codequality
codesniffer
consitent
corepack
ctools
curlopt
cweagans
dburl
ddev
dealerdirect
defininition
dockerhub
dotenv
drupalspoons
drupaltestbot
drupaltestbotpw
drush
elif
flagword
flagwords
gitlabci
gnomovision
interruptible
keycdn
ksection
lando
miminum
overriden
percona
pgsql
phpcodesniffer
phpspec
phpunit's
prelease
quicklinks
returntransfer
ruleset
simpletest
stylelintignore
stylelintrc
symfony
testgroups
unrecognised
vset
webide
webserver
wodby
yoyodyne
squidfunk
drupalcode
nextmajor
Dwebdriver
logfile
XVFB
