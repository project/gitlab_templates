module.exports = {
    root: true,
    extends: [
        "plugin:prettier/recommended",
        "plugin:yml/recommended"
    ],
    rules: {
        "yml/no-empty-mapping-value": "off",
        "yml/plain-scalar": "off",
        "no-trailing-spaces": 2,
    },
    env: {
        browser: true,
        es2024: true,
        node: true
    }
}
